/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2002, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#ifndef __have_driver_h__
#define __have_driver_h__

#include <linux/skbuff.h>
#include <linux/pci.h>
#include <linux/spinlock.h>
#include <linux/capi.h>
#include <linux/isdn/capilli.h>
#include "tables.h"
#include "queue.h" 
#include "libdefs.h" 
#include "common.h"

struct fritz_card;

struct fritz_ctrl {
	struct capi_ctr		ctr;
	char			*version;
	char			*string[8];
	struct fritz_card	*card;
#ifdef DEBUG
	int			cid;
#endif
};

struct fritz_card {
	/* Software */
	struct fritz_ctrl	*ctrl[2];
	unsigned		length;
	appltab_t		appls;
	queue_t			queue;
	bool			running;

	void (asmlinkage *		reg_func) (void *, unsigned);
	void (asmlinkage *		rel_func) (void *);
	void (asmlinkage *		dwn_func) (void);

	/* Hardware */
	c6205_context		c6205_ctx;
	ioaddr_t		iowin[3];

	unsigned long		addr_data;
	unsigned long		len_data;
	void *			data_base;

	unsigned long		addr_code;
	unsigned long		len_code;
	void *			code_base;

	unsigned long		addr_pio;
	unsigned long		len_pio;
	unsigned		pio_base;

	int			irq;

	/* TX */
	void *			tx_dmabuf;
	void *			tx_dmabuf_b;
	void *			tx_base;
	void *			tx_buffer;
	unsigned		tx_length;
	hw_completion_func_t	tx_func;
	void *			tx_ctx;
	
	/* RX */
	void *			rx_dmabuf;
	void *			rx_dmabuf_b;
	void *			rx_base;
	void *			rx_buffer;
	unsigned		rx_length;
	hw_completion_func_t	rx_func;
	void *			rx_ctx;

	unsigned		pc_ack_offset;
	unsigned		timer_offset;

	struct task_struct	*stack_thread;
	bool			xfer_pending;
	bool			scheduler_kicked;
	bool			terminating;
	spinlock_t		kick_lock;
};

static inline struct fritz_ctrl *capi2fritz_ctrl(struct capi_ctr *ctr)
{
	return ctr->driverdata;
}

static inline struct fritz_card *capi2fritz_card(struct capi_ctr *ctr)
{
	return capi2fritz_ctrl(ctr)->card;
}

extern struct fritz_card *capi_card;
extern struct mutex stack_lock;

void remove_ctrls(struct fritz_card *card);
int add_card(struct pci_dev *dev);

int nbchans(struct capi_ctr *ctrl);

int msg2stack(unsigned char *msg);
void msg2capi(unsigned char *msg);

unsigned get_timer(struct fritz_card *card);
void pc_acknowledge(struct fritz_card *card, unsigned char num);

void kick_stack_thread(struct fritz_card *card, bool kick_xfer,
		       bool kick_scheduler);

void init(unsigned, void (asmlinkage *)(void *, unsigned),
	  void (asmlinkage *)(void *), void (asmlinkage *)(void));

int fcdsl2_driver_init(void);
void fcdsl2_driver_exit(void);

#endif
