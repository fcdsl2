/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2003, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#ifndef __have_fw_h__
#define __have_fw_h__

#include "libdefs.h"
#include "common.h"

asmlinkage ioaddr_p fw_get_window(unsigned); /* unused */

asmlinkage c6205_context fw_init(void *, void *, unsigned, unsigned, ioaddr_p,
				 int *);
asmlinkage void fw_exit(c6205_context *);

asmlinkage int fw_ready(void);
asmlinkage int fw_setup(c6205_context);
asmlinkage int fw_send_reset(c6205_context);
asmlinkage unsigned fw_get_timer_offset(c6205_context); /* unused */

#endif
