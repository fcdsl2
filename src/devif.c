/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2003, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#include <linux/io.h>
#include <linux/sched.h>
#include <linux/spinlock.h>
#include "tools.h"
#include "lib.h"
#include "fw.h"	
#include "driver.h"
#include "devif.h"

#define	DMA_BUFFER_NUM_MAX	8
#define	DMA_INVALID_NUMBER	0
#define	DMA_ALIGNMENT		8
#define	DMA_ALIGN_MASK		(DMA_ALIGNMENT-1)
#define	MAX_HEADER_RETRANSMIT	100
#define	MAX_LENGTH		16 * 1024

#define	INC(x)			(x)=(((x)==255)?1:(x)+1)

#define	PCI_INT_LINE_ON(x)	(((x)&C6205_PCI_HSR_INTAVAL)!=0)

static bool			window_ok;
#ifdef DEBUG
static bool			is_stopped;
static atomic_t			is_open			= ATOMIC_INIT (0);
static atomic_t			is_started		= ATOMIC_INIT (0);
static atomic_t			n_buffers		= ATOMIC_INIT (0);
static unsigned			last_to_pc_ack;
static unsigned			tx_handled_count	= 0;
static unsigned			rx_handled_count	= 0;
#endif /* DEBUG */

static hw_completion_func_t	rx_callback		= NULL;
static hw_completion_func_t	tx_callback		= NULL;
static hw_event_func_t		event_callback		= NULL;
static xfer_func_t		tx_function		= NULL;
static dif_require_t		dif_req			= { 0, };
static unsigned			buf_disc		= 0;
static ptr_queue_p		tx_list			= NULL;
static ptr_queue_p		rx_list			= NULL;
static unsigned char		tx_num			= 1;
static unsigned char		rx_num			= 1;
static unsigned			rx_hdr_fail;
static unsigned			rx_blk_fail;
static unsigned			crrx_local;
static unsigned			crrx_global;
static unsigned			ro_num;
static unsigned			ro_offset;
static ioaddr_p			ro_pwin;
static unsigned			ro_len;
static unsigned			ro_next;
static unsigned			ro_bufofs;

typedef struct __rbi {
	hw_buffer_descriptor_p	pbd;
	unsigned		ofs;
	unsigned		len;
	void *			ctx;
} buf_info_t, * buf_info_p;

/*****************************************************************************\
 * D M A   B U F F E R   M A N A G E M E N T
\*****************************************************************************/ 

struct __ndi {

	unsigned char		num;
	unsigned char		cr;
	unsigned short		len;
	unsigned		timer;
} __attribute__ ((packed));

static dma_struct_p		dma_struct[2 * DMA_BUFFER_NUM_MAX];
#ifdef DEBUG
static dma_struct_p		tx_last_buffer = NULL;
static dma_struct_p		rx_last_buffer = NULL;
#endif

dma_struct_p *dma_get_struct_list(unsigned *pnum)
{
#ifdef LOG_LINK
	unsigned	n;

	fritz_dbg("List of %u DMA structures: %p\n", buf_disc, dma_struct);
	for (n = 0; n < buf_disc; n++) {
		assert (dma_struct[n] != NULL);
		fritz_dbg("%02u) %cX vaddr/paddr = %lx/%lx\n", n,
			  (dma_struct[n]->direction == DMA_TX) ? 'T' : 'R',
			  dma_struct[n]->virt_addr, dma_struct[n]->phys_addr);
	}
#endif /* LOG_LINK */
	assert (pnum != NULL);
	*pnum = buf_disc;
	return dma_struct;
}

dma_struct_p dma_alloc(void)
{
	dma_struct_p pdma;

	pdma = kzalloc(sizeof(dma_struct_t), GFP_KERNEL);
	info (pdma != NULL);
	return pdma;
}

void dma_free(dma_struct_p *ppdma)
{
	assert (ppdma != NULL);
	assert (*ppdma != NULL);
	assert ((*ppdma)->virt_addr == 0UL);

	kfree(*ppdma);
	*ppdma = NULL;
}

static unsigned dma_min_buf_size(dma_struct_p pdma, enum dma_dir direction)
{
	unsigned bs;

	if (pdma != NULL) {
		direction = pdma->direction;
	}
	bs = sizeof (num_dma_info_t) + sizeof (unsigned);
	if (direction == DMA_RX) {
		bs += sizeof(unsigned);
	}
	return bs;
}

static unsigned dma_eff_buf_size(dma_struct_p pdma)
{
	assert (pdma != NULL);
	assert (pdma->length >= dma_min_buf_size(pdma, DMA_RX));
	return pdma->length - dma_min_buf_size(pdma, DMA_RX);
}

static char *dma_eff_buf_virt(dma_struct_p pdma)
{
	assert (pdma != NULL);
	return ((char *) pdma->virt_addr) + sizeof (num_dma_info_t);
}

static void dma_reset_buf(dma_struct_p pdma)
{
	assert (pdma != NULL);
	memset((void *)pdma->virt_addr, 0, pdma->length);
	fritz_dbg("DMA buffer 0x%lx reset\n", pdma->virt_addr);
}

static void dma_invalidate(dma_struct_p pdma)
{
	num_dma_info_p	pndi = (num_dma_info_p) pdma->virt_addr;
	unsigned short	len;

	assert (pdma != NULL);
	assert (pndi != NULL);
	len = pdma->length;
	assert (len >= dma_min_buf_size(pdma, DMA_RX));
	assert (DMA_INVALID_NUMBER == 0);
	assert (len >= sizeof (num_dma_info_t));

	memset(pndi, 0, len);
}

bool dma_setup(dma_struct_p pdma, unsigned buf_size)
{
	void *buf1;
	void *buf2;

	assert (buf_size > dma_min_buf_size(pdma, DMA_RX));
	assert (buf_size > 0);
	assert (pdma != NULL);
	assert (pdma->virt_addr == 0UL);

	buf1 = kmalloc(buf_size + DMA_ALIGNMENT, GFP_KERNEL);
	if (!buf1) {
		pdma->virt_addr = 0UL;
		return false;
	}
	if (((long)buf1) & DMA_ALIGN_MASK) {
		fritz_dbg("Buffer is not %d-aligned (%p)\n", DMA_ALIGNMENT,
			  buf1);
		buf2 = (void *)
			((((long)buf1) + DMA_ALIGNMENT - 1) & ~DMA_ALIGN_MASK);
	} else {
		buf2 = buf1;
	}
	assert ((((long)buf2) & DMA_ALIGN_MASK) == 0);
	memset(buf2, 0, buf_size);
	pdma->length	= buf_size;
	pdma->alloc_ptr = buf1;
	pdma->virt_addr = (long)buf2;
	pdma->phys_addr = (long)virt_to_bus(buf2);
	fritz_dbg("Allocated DMA buffer vaddr %lx, paddr %lx, length %u\n",
		  pdma->virt_addr, pdma->phys_addr, pdma->length);
	assert (pdma->length == buf_size);
	return true;
}

static bool
dma_init(dma_struct_p pdma, unsigned buf_size, enum dma_dir direction)
{
	if (!dma_setup(pdma, buf_size))
		return false;

	dma_invalidate(pdma);
	pdma->direction = direction;
	fritz_dbg("... is a %s DMA buffer\n",
		  (pdma->direction == DMA_TX) ? "TX" : "RX");

	return true;
}

static void dma_free_buffer(dma_struct_p pdma)
{
	assert (pdma != NULL);
	info (pdma->virt_addr != 0UL);
	if (pdma->virt_addr != 0UL) {
		kfree (pdma->alloc_ptr);
		pdma->virt_addr = pdma->phys_addr = 0UL;
	}
}

void dma_exit(dma_struct_p pdma)
{
	assert (pdma != NULL);
	info (pdma->virt_addr != 0UL);
	if (pdma->virt_addr != 0UL) {
		fritz_dbg("Freeing %s DMA buffer vaddr %lx\n",
			  (pdma->direction == DMA_TX) ? "TX" : "RX",
			  pdma->virt_addr);
		dma_invalidate(pdma);
		dma_free_buffer(pdma);
	}
}

static unsigned char *ndi_number_back_ptr(dma_struct_p pdma, unsigned len)
{
	num_dma_info_p	pndi = (num_dma_info_p) pdma->virt_addr;
	unsigned	ofs;
	
	assert (pdma != NULL);
	assert (pndi != NULL);
	ofs = len + sizeof (num_dma_info_t);
	assert ((ofs + sizeof (unsigned char)) < pdma->length);
	return ((unsigned char *) pndi) + ofs; 
}

static bool
ndi_length_valid(dma_struct_p pdma, unsigned short len, unsigned short maxlen)
{
	num_dma_info_p	pndi = (num_dma_info_p) pdma->virt_addr;

	assert (pdma != NULL);
	assert (pndi != NULL);
	if (((pndi->len) & 3) != 0) {
		info (0);
		return false;
	}
	if (len > maxlen) {
		info (0);
		return false;
	}
	return true;
}

#ifdef DEBUG
static bool ndi_header_valid(dma_struct_p pdma, unsigned short maxlen)
{
	num_dma_info_p	pndi = (num_dma_info_p) pdma->virt_addr;

	assert (pdma != NULL);
	assert (pndi != NULL);
	if (pndi->num == DMA_INVALID_NUMBER) {
		info (0);
		return false;
	}
	return ndi_length_valid(pdma, pndi->len, maxlen);
}

static bool ndi_buffer_valid(dma_struct_p pdma)
{
	num_dma_info_p pndi = (num_dma_info_p)pdma->virt_addr;

	assert (pdma != NULL);
	assert (pndi != NULL);

	if (!ndi_header_valid(pdma, dma_eff_buf_size(pdma)))
		return false;

	if (pdma->direction == DMA_RX) {
		unsigned char *uc = ndi_number_back_ptr(pdma, pndi->len);

		if (pndi->num != *uc)
			return false;
	}
	return true;
}
#endif /* DEBUG */

static void dma_validate_cs(dma_struct_p pdma, unsigned char num, unsigned cs)
{
	num_dma_info_p pndi = (num_dma_info_p)pdma->virt_addr;
	long bnum;

	assert (pdma != NULL);
	assert (pndi != NULL);
	bnum = 3 + (long)ndi_number_back_ptr(pdma, pndi->len);
	bnum &= ~3;

	assert ((bnum + sizeof (unsigned)) <= (pdma->virt_addr + pdma->length));
	if ((bnum + sizeof (unsigned)) > (pdma->virt_addr + pdma->length))
		fritz_dbg("Back num 0x%lx, buffer (virtual) 0x%lx\n",
			  bnum, pdma->virt_addr);

	*((unsigned *) bnum) = cs;
	assert (num != DMA_INVALID_NUMBER);
	pndi->num = num;
	assert (ndi_buffer_valid (pdma));
}

static void dma_validate(dma_struct_p pdma, unsigned char cr,
			 unsigned short len, unsigned timer)
{
	num_dma_info_p pndi = (num_dma_info_p) pdma->virt_addr;

	assert (pdma != NULL);
	assert (pndi != NULL);
	assert (len <= dma_eff_buf_size (pdma));
	assert ((len+dma_min_buf_size(pdma, pdma->direction)) <= pdma->length);

	pndi->cr	= cr;
	pndi->len	= len;
	pndi->timer	= timer;
}

static unsigned char ndi_get_cr(dma_struct_p pdma)
{
	num_dma_info_p	pndi = (num_dma_info_p) pdma->virt_addr;

	assert (pdma != NULL);
	assert (pndi != NULL);
	return pndi->cr;
}

static unsigned short ndi_get_length(dma_struct_p pdma)
{
	num_dma_info_p	pndi = (num_dma_info_p) pdma->virt_addr;

	assert (pdma != NULL);
	assert (pndi != NULL);
	return pndi->len;
}

static void ndi_set_number(dma_struct_p pdma, unsigned char x)
{
	num_dma_info_p pndi = (num_dma_info_p) pdma->virt_addr;

	assert (pdma != NULL);
	assert (pndi != NULL);
	pndi->num = x;
}

static unsigned char ndi_get_number(dma_struct_p pdma)
{
	num_dma_info_p pndi = (num_dma_info_p) pdma->virt_addr;

	assert (pdma != NULL);
	assert (pndi != NULL);
	return pndi->num;
}

/*****************************************************************************\
 * H E L P E R   F U N C T I O N S
\*****************************************************************************/

static unsigned check_sum(unsigned char num, unsigned wcnt, unsigned *addr)
{
	unsigned	sum = 0x55aa55aa;

	assert (addr != NULL);
	assert (wcnt > 0);
#if defined (LOG_CHECKSUM)
	fritz_dbg("[start] sum = 0x%x\n", sum);
#endif
	sum ^= (unsigned) num;
#if defined (LOG_CHECKSUM)
	fritz_dbg("[number] sum = 0x%x, num = 0x%x\n", sum, num);
#endif
	do {
		sum ^= *addr;
#if defined (LOG_CHECKSUM)
		fritz_dbg("[%u] sum = 0x%x, num = 0x%x\n", wcnt, sum, *addr);
#endif
		++addr;
	} while (--wcnt);
	return sum;
}

static void trigger_interrupt(struct fritz_card *card)
{
	outl(C6205_PCI_HDCR_DSPINT, card->pio_base + C6205_PCI_HDCR_OFFSET);
}

#if 0 && defined(LOG_LINK)
static void dump_buffers(enum dma_dir direction) {
	unsigned	n, ofs;
	unsigned	offset, val0, val1;
	
	ofs = (direction == DMA_TX) ? 0 : DMA_BUFFER_NUM_MAX;
	fritz_dbg("Dump buffers in %cX direction...\n",
		  (direction == DMA_TX) ? 'T' : 'R');
	for (n = ofs; n < (ofs + DMA_BUFFER_NUM_MAX); n++) {
		if (dma_struct[n] != NULL) {
			memdump (
				(void *) dma_struct[n]->virt_addr, 
				32, 
				dma_struct[n]->virt_addr,
				(direction == DMA_TX) ? "TX buffer" : "RX buffer"
			);
		}
		if (direction == DMA_RX && (ro_pwin != NULL)) {
			offset = ro_offset + (n - 8) * ro_len + ro_bufofs;
			val0 = ro_pwin->inl (ro_pwin->virt_addr + offset);
			val1 = ro_pwin->inl (ro_pwin->virt_addr + offset + 4);
			fritz_dbg("RX%u prefix: %08X %08X\n", n, val0, val1);
		}
	}
	fritz_dbg("End of dump\n");
}
#endif

/*****************************************************************************\
 * T R A N S F E R   M A N A G E M E N T
\*****************************************************************************/ 

static bool receive_buf(struct fritz_card *card, dma_struct_p pdma)
{
	unsigned	count = 0;
	unsigned	offset, chksum;
	unsigned	wc, wcnt, tmp1;
	unsigned *	mem;
	unsigned *	tmp2;
	int		res;

	assert (pdma != NULL);
	assert (ro_pwin != NULL);
	offset = ro_offset + ro_next * ro_len + ro_bufofs;
	mem = (unsigned *) pdma->virt_addr;
	assert (mem != NULL);
	do {
		count++;
		*mem	   = ro_pwin->inl (ro_pwin->virt_addr + offset);
		*(mem + 1) = ro_pwin->inl (ro_pwin->virt_addr + offset + sizeof (unsigned));
	} while ((*mem != ~*(mem + 1)) && (count < MAX_HEADER_RETRANSMIT));
	if (count > 2) {
		rx_hdr_fail += count - 2;
	}
	if (count > MAX_HEADER_RETRANSMIT) {
		fritz_dbg("Too many header re-transmits (%u)...\n", count);
		return false;
	}
	if (ndi_get_number (pdma) != rx_num) {
#if 0 && defined(LOG_LINK)
		fritz_dbg("Unexpected number, want:%u, have:%u...\n", rx_num,
			  ndi_get_number (pdma));
#endif
		return false;
	}
	res = ndi_length_valid (pdma, ndi_get_number (pdma), dma_eff_buf_size (pdma));
	if (!res) {
#ifdef LOG_LINK
		fritz_dbg("Invalid length (%d)...\n", ndi_get_length (pdma));
#endif
		return false;
	}
	assert (0 == (sizeof (num_dma_info_t) % 4));
	count = 0;
	offset += sizeof (num_dma_info_t);

	/* wcnt: datalen + num + checksum */
	wcnt = (ndi_get_length (pdma) + 4 + 4) >> 2;
	tmp1 = sizeof (num_dma_info_t) >> 2;
	tmp2 = mem + tmp1;
	do {
		count++;
		for (wc = 0; wc < wcnt; wc++) {
			*(tmp2 + wc) = ro_pwin->inl (ro_pwin->virt_addr + offset + 4 * wc);
		}
		chksum = check_sum (0, wcnt + tmp1, mem);
	} while ((chksum != 0) && (count < MAX_HEADER_RETRANSMIT));
	if (count > 1) {
		rx_blk_fail += count - 1;
	}
	if (count > MAX_HEADER_RETRANSMIT) {
		fritz_dbg("Too many block re-transmits (%u)...\n", count);
		return false;
	}
	assert (ndi_buffer_valid (pdma));
	return true;
}

static void receive_done(void)
{
	if (++ro_next == dif_req.rx_block_num)
		ro_next = 0;
}

static unsigned int_complete_buf(enum dma_dir direction)
{
	ptr_queue_p 		pq;
	hw_completion_func_t	pcf;
	dma_struct_p		pdma;
	buf_info_p		pinfo;
	buf_info_t		info;
	void *			phlp;
	bool			success;

	if (direction == DMA_TX) {
		pq  = tx_list;
		pcf = tx_callback;
	} else {
		pq  = rx_list;
		pcf = rx_callback;
	}
	phlp = NULL;
	success = q_dequeue(pq, &phlp);
	assert (success);
	pinfo = (buf_info_p) phlp;
	assert (pinfo != NULL);
	info = *pinfo;
	
	assert (info.pbd != NULL);
	assert (info.ofs == 0);
	pdma = (dma_struct_p) info.pbd->context;

	assert (pdma != NULL);
	assert (info.len <= dma_eff_buf_size (pdma));
	assert (ndi_get_length (pdma) <= dma_eff_buf_size (pdma));

	assert (ndi_buffer_valid (pdma));
	assert (pcf != NULL);
	(* pcf) (info.pbd, info.ofs, ndi_get_length (pdma), info.ctx);
	return 1;
}

void xfer_handler(struct fritz_card *card)
{
	unsigned	tx_handled	= 0;
	unsigned	rx_handled	= 0;
	void *		phlp		= NULL;
	buf_info_p	pinfo;
	unsigned	cr;
	dma_struct_p	pdma;
	
	while (q_peek (rx_list, &phlp)) {
		pinfo = (buf_info_p) phlp;
		assert (pinfo != NULL);
		assert (pinfo->pbd != NULL);
		pdma = (dma_struct_p) pinfo->pbd->context;
		assert (pdma != NULL);
		if (!window_ok) {
			fritz_dbg("I/O window not ready!\n");
			break;
		} 
		if (!receive_buf(card, pdma))
			break;
#if 0 && defined(LOG_LINK)
		fritz_dbg("Received buf #%u\n", ndi_get_number (pdma));
#endif
		assert (ndi_get_number (pdma) == rx_num);
		INC(rx_num);
		receive_done ();

		cr = ndi_get_cr (pdma);
		rx_handled += int_complete_buf(DMA_RX);
		while (cr-- > 0)
			tx_handled += int_complete_buf(DMA_TX);

		phlp = NULL;
	}
#ifdef DEBUG
	assert ((tx_handled == 0) || (rx_handled > 0));
	tx_handled_count += tx_handled;
	rx_handled_count += rx_handled;
#endif
	if (tx_handled || rx_handled)
		kick_stack_thread(card, false, true);
}

irqreturn_t device_interrupt(int irq, void *args)
{
	unsigned long status;
	struct fritz_card *card = args;

	status = inl(card->pio_base + C6205_PCI_HSR_OFFSET);
	if (!(status & C6205_PCI_HSR_INTSRC))
		return IRQ_NONE;

	do {
		outl(C6205_PCI_HSR_INTSRC,
		     card->pio_base + C6205_PCI_HSR_OFFSET);
		status = inl(card->pio_base + C6205_PCI_HSR_OFFSET);
	} while (status & C6205_PCI_HSR_INTSRC);

	card->xfer_pending = true;
	kick_stack_thread(card, true, false);

	return IRQ_HANDLED;
}

/*****************************************************************************\
 * D E V I C E   I N T E R F A C E
\*****************************************************************************/ 

asmlinkage void dif_xfer_requirements(dif_require_p rp, unsigned ofs)
{
	assert (rp != NULL);
	assert (rp->tx_block_num > 0);
	assert (rp->tx_block_size > 0);
	assert (rp->tx_max_trans_len == 0);
	assert (rp->rx_block_num > 0);
	assert (rp->rx_block_size > 0);
	assert (rp->rx_max_trans_len == 0);
	assert (rp->tx_block_num <= DMA_BUFFER_NUM_MAX);
	assert (rp->rx_block_num <= DMA_BUFFER_NUM_MAX);

	dif_req = *rp;
	assert (dif_req.tx_block_size >= dma_min_buf_size(NULL, DMA_TX));
	dif_req.tx_max_trans_len =
		dif_req.tx_block_size - dma_min_buf_size(NULL, DMA_TX);
	assert (dif_req.rx_block_size >= (dma_min_buf_size(NULL, DMA_RX)+ofs));
	dif_req.rx_max_trans_len =
		dif_req.rx_block_size - dma_min_buf_size(NULL, DMA_RX) - ofs;
	dif_req.tx_block_size += 4 * sizeof (unsigned char);
	
#ifdef LOG_LINK
	fritz_dbg("Requirements: TX %u/%u/%u, RX %u/%u/%u, Func/Ctx %p/%p\n",
		  dif_req.tx_block_num,
		  dif_req.tx_block_size,
		  dif_req.tx_max_trans_len,
		  dif_req.rx_block_num,
		  dif_req.rx_block_size,
		  dif_req.rx_max_trans_len,
		  dif_req.get_code,
		  dif_req.context);
#endif
}

static void dif_reset(void)
{
	fritz_dbg("Resetting device structures...\n");

	window_ok	= false;
	ro_num		= 0;
	ro_offset	= 0;
	ro_pwin		= NULL;
	ro_len		= 0;
	ro_next		= 0;
	ro_bufofs	= 0;
	rx_hdr_fail	= 0;
	rx_blk_fail	= 0;

	q_reset (tx_list);
	q_reset (rx_list);
	crrx_local = 0;
	crrx_global = 0;
	tx_num = 1;
	rx_num = 1;
#ifdef DEBUG
	tx_last_buffer = 0;
	rx_last_buffer = 0;
	tx_handled_count = 0;
	rx_handled_count = 0;
	is_stopped = false;
#endif /* DEBUG */
	fritz_dbg("Reset done.\n");
}

void dif_set_params(unsigned num, unsigned offset, ioaddr_p pwin,
		    unsigned len, unsigned bufofs)
{
#if defined(DEBUG) && defined(LOG_LINK)
	unsigned	n, ofs;
#endif
	assert (num > 0);
	assert ((offset % 4) == 0);
	assert (len > 0);
	assert ((len % 4) == 0);
	assert (pwin != NULL);

	ro_num = num;
	ro_offset = offset;
	ro_pwin = pwin;
	ro_len = len;
	ro_bufofs = bufofs;
	window_ok = true;
	fritz_dbg("I/O window has been set!\n");

#if defined(DEBUG) && defined(LOG_LINK)
	fritz_dbg("dif_set_params:\n");
	for (n = 0; n < ro_num; n++) {
		ofs = ro_offset + n * ro_len + ro_bufofs;
		fritz_dbg("buffer(%u), vaddr 0x%08x\n", n,
			  pwin->virt_addr + ofs);
	}
#endif
}

bool dif_init(void)
{
	fritz_dbg("Initializing device structures...\n");

	buf_disc	= 0;
	ro_num		= 0;
	ro_offset	= 0;
	ro_pwin		= NULL;
	ro_len		= 0;
	ro_next		= 0;
	ro_bufofs	= 0;
	rx_hdr_fail	= 0;
	rx_blk_fail	= 0;
	tx_num		= 1;
	rx_num		= 1;
	crrx_local	= 0;
	crrx_global	= 0;

	tx_list = q_make(DMA_BUFFER_NUM_MAX);
	if (!tx_list) {
		fritz_dbg("Could not allocate TX queue\n");
		return false;
	}
	rx_list = q_make(DMA_BUFFER_NUM_MAX);
	if (!rx_list) {
		fritz_dbg("Could not allocate RX queue\n");
		q_remove (&tx_list);
		return false;
	}
	if (!q_attach_mem(tx_list, DMA_BUFFER_NUM_MAX, sizeof(buf_info_t)) ||
	    !q_attach_mem(rx_list, DMA_BUFFER_NUM_MAX, sizeof(buf_info_t))) {
		q_remove (&tx_list);
		q_remove (&tx_list);
		return false;
	}

	memset(&dma_struct, 0, 2 * DMA_BUFFER_NUM_MAX * sizeof(dma_struct_p));
	memset(&dif_req, 0, sizeof(dif_require_t));
	return true;
}

void dif_exit(void)
{
	dif_reset ();
	fritz_dbg("Deinitializing device...\n");
	if (tx_list != NULL) {
		q_remove (&tx_list);
	}
	if (rx_list != NULL) {
		q_remove (&rx_list);
	}
}

static void dif_stop(enum dma_dir direction)
{
	void *		phlp = NULL;
	buf_info_p	pi;
	ptr_queue_p	pq;
	dma_struct_p	pdma;
	unsigned	n;

	fritz_dbg("Stopping device interface (%d)...\n", direction);
	pq = (direction == DMA_TX) ? tx_list : rx_list;
	assert (pq != NULL);
	while (q_dequeue(pq, &phlp)) {
		pi = (buf_info_p) phlp;
		assert (pi != NULL);
		assert (pi->pbd != NULL);
		pdma = (dma_struct_p) pi->pbd->context;
		assert (pdma != NULL);

		dma_invalidate(pdma);
		assert (event_callback != NULL);
		(*event_callback)(DMA_REQUEST_CANCELLED, pi->pbd, pi->ofs,
				  pi->len, pi->ctx);
		
		phlp = NULL;
	}
	for (n = 0; n < (2 * DMA_BUFFER_NUM_MAX); n++) {
		info (dma_struct[n] != NULL);
		if (dma_struct[n] != NULL) {
			dma_reset_buf (dma_struct[n]);
		}
	}
}

/*****************************************************************************\
 * H A R D W A R E   I N T E R F A C E
 *
 * Note: Apparently, these functions are directly called by the binary blob -
 *       the usual inconsistency...
\*****************************************************************************/

asmlinkage unsigned OSHWBlockOpen(unsigned index, hw_block_handle *handle)
{
	assert (handle != NULL);
	info (*handle == NULL);
	*handle = (hw_block_handle) capi_card;
	assert (!atomic_xchg (&is_open, 1));
	return 1;
}

asmlinkage void OSHWBlockClose(hw_block_handle handle)
{
	assert (atomic_read (&n_buffers) == 0);
	assert (atomic_read (&is_open));
	assert (!atomic_read (&is_started));
	assert (atomic_xchg (&is_open, 0));
}

asmlinkage unsigned
OSHWStart(hw_block_handle handle, hw_completion_func_t rx_completer,
	  hw_completion_func_t tx_completer, hw_event_func_t event_handler)
{
	assert (atomic_read (&is_open));
	assert (!atomic_read (&is_started));
	assert (rx_callback == NULL);
	assert (tx_callback == NULL);
	assert (event_callback == NULL);

	rx_callback	= rx_completer;
	tx_callback	= tx_completer;
	event_callback	= event_handler;
	assert (!atomic_xchg (&is_started, 1));

	return 1;
}

asmlinkage void OSHWStop(hw_block_handle handle)
{
	assert (atomic_read (&is_open));
	assert (atomic_read (&is_started));

	dif_stop(DMA_TX);
	dif_stop(DMA_RX);
#ifdef DEBUG
	is_stopped = true;
#endif

	rx_callback	= NULL;
	tx_callback	= NULL;
	event_callback	= NULL;
	assert (atomic_xchg (&is_started, 0));
}

asmlinkage hw_buffer_descriptor_p
OSHWAllocBuffer(hw_block_handle handle, unsigned length)
{
	hw_buffer_descriptor_p	pbd;
	dma_struct_p		pstr;
#ifdef DEBUG
	unsigned		start, stop, n;
#endif
	
	fritz_dbg("Allocating DMA buffer of %u bytes\n", length);
	assert (atomic_read (&is_open));
	assert (length < OSHWGetMaxBlockSize (handle));
	assert (dif_req.tx_block_num > 0);
	assert (dif_req.rx_block_num > 0);
	assert ((length == dif_req.tx_block_size) || (length == dif_req.rx_block_size));

	pbd = kmalloc(sizeof(hw_buffer_descriptor_t), GFP_KERNEL);
	if (!pbd)
		return NULL;

	pstr = dma_alloc();
	if (!pstr) {
		kfree(pbd);
		return NULL;
	}

	if (!dma_init(pstr, length, (buf_disc < dif_req.tx_block_num))) {
		dma_exit(pstr);
		dma_free(&pstr);
		kfree(pbd);
		return NULL;
	}

	pbd->handle	= handle;
	pbd->buffer	= dma_eff_buf_virt (pstr);
	pbd->length	= length;
	pbd->context	= pstr;

	assert (buf_disc < 2 * DMA_BUFFER_NUM_MAX);
	assert (dma_struct[buf_disc] == NULL);
	dma_struct[buf_disc++] = pstr;

#ifdef DEBUG
	atomic_inc (&n_buffers);
	start = stop = 0;
	if (buf_disc == dif_req.tx_block_num) {
		stop  = buf_disc;
	} else if (buf_disc == (dif_req.tx_block_num + dif_req.rx_block_num)) {
		start = dif_req.tx_block_num;
		stop  = buf_disc;
	}
	if (start != stop) {
		assert (stop > 1);
		for (n = start; n < (stop - 1); n++) {
			assert (dma_struct[n] != NULL);
			assert (dma_struct[n + 1] != NULL);
			dma_struct[n]->context = dma_struct[n + 1];
			fritz_dbg("DMA context of structure "
				  "#%u(%p) set to %p\n", n, dma_struct[n],
				  dma_struct[n + 1]);
		}
		dma_struct[n]->context = dma_struct[start];
		fritz_dbg("DMA context of structure #%u(%p) set to %p\n",
			  n, dma_struct[n], dma_struct[start]);
	}
#endif /* DEBUG */

	return pbd;
}

asmlinkage void OSHWFreeBuffer(hw_buffer_descriptor_p desc)
{
	dma_struct_p	pstr;

	if (desc == NULL) {
		fritz_dbg("Attempt to free NULL descriptor!\n");
		return;
	}
	pstr = (dma_struct_p) desc->context;
	assert (pstr != NULL);
	desc->buffer = (unsigned char *) pstr->virt_addr;
	
	assert (atomic_read (&is_open));
	assert (atomic_read (&n_buffers) > 0);
	dma_exit(pstr);
	kfree(pstr);
	kfree(desc);

	assert (buf_disc > 0);
	assert (buf_disc <= 2 * DMA_BUFFER_NUM_MAX);
	buf_disc--;
	assert (dma_struct[buf_disc] != NULL);
	dma_struct[buf_disc] = NULL;
#ifdef DEBUG
	atomic_dec (&n_buffers);
#endif
}

asmlinkage unsigned OSHWGetMaxBlockSize(hw_block_handle handle)
{
	return 4 * 1024;
}

asmlinkage unsigned OSHWGetMaxConcurrentRxBlocks(hw_block_handle handle)
{
	return dif_req.rx_block_num;
}

asmlinkage unsigned OSHWGetMaxConcurrentTxBlocks(hw_block_handle handle)
{
	return dif_req.tx_block_num;
}

asmlinkage void *
OSHWExchangeDeviceRequirements(hw_block_handle handle, void *pin)
{
	in_require_p	pr = (in_require_p) pin;

	info (pr != NULL);
	if (pr != NULL) {
		tx_function = pr->tx_func;
		assert (tx_function != NULL);
	}
	assert (dif_req.tx_block_num > 0);
	assert (dif_req.tx_block_size > 0);
	assert (dif_req.tx_max_trans_len > 0);
	assert (dif_req.rx_block_num > 0);
	assert (dif_req.rx_block_size > 0);
	assert (dif_req.rx_max_trans_len > 0);
	fritz_dbg("Returning requirements, TX %u/%u/%u, RX %u/%u/%u\n", 
		  dif_req.tx_block_num,
		  dif_req.tx_block_size,
		  dif_req.tx_max_trans_len,
		  dif_req.rx_block_num,
		  dif_req.rx_block_size,
		  dif_req.rx_max_trans_len
	);
	return &dif_req;
}

asmlinkage unsigned
OSHWTxBuffer(const hw_buffer_descriptor_p desc, unsigned offset,
	     unsigned length, void *context)
{
	buf_info_t info = { desc, offset, length, context };
	dma_struct_p pdma;
	unsigned chksum, timer = 0;
	int res;

	assert (!is_stopped);
	assert (atomic_read (&is_open));
	assert (info.pbd != NULL);
	pdma = (dma_struct_p) info.pbd->context;
	assert (pdma != NULL);
	assert (info.ofs == 0);
	assert (info.len <= dif_req.tx_max_trans_len);
	assert (info.len <= dma_eff_buf_size (pdma));
	assert (info.len <= MAX_LENGTH);
	
	assert (q_get_count (tx_list) < dif_req.tx_block_num);

#ifdef DEBUG
	timer = get_timer(desc->handle);
#endif
	dma_validate (pdma, 0, info.len, timer);
	ndi_set_number (pdma, 0);	/* 0 for chksum */
	chksum = check_sum (
			tx_num, 
			(sizeof (num_dma_info_t) + ndi_get_length (pdma) + 3) >> 2, 
			(unsigned *) pdma->virt_addr
		 );
	dma_validate_cs (pdma, tx_num, chksum);
	res = q_enqueue_mem (tx_list, &info, sizeof (buf_info_t));
	assert (res != 0);
	assert (q_get_count (tx_list) <= DMA_BUFFER_NUM_MAX);
	assert (q_get_count (tx_list) <= dif_req.tx_block_num);
#ifdef DEBUG
	if (tx_last_buffer != NULL) {
		assert ((void *) pdma == tx_last_buffer->context);
	} else {
		assert (pdma == dma_struct [0]);
	}
	tx_last_buffer = pdma;
#endif /* DEBUG */
	INC(tx_num);
	trigger_interrupt(desc->handle);
	
	return 1;
}

asmlinkage unsigned
OSHWRxBuffer(const hw_buffer_descriptor_p desc, unsigned offset,
	     unsigned length, void *context)
{
	buf_info_t			info	= { desc, offset, length, context } ;
	dma_struct_p			pdma;
	int				res;
#ifdef DEBUG
	unsigned			time;
#endif

	assert (!is_stopped);
	assert (atomic_read (&is_open));
	assert (info.pbd != NULL);
	pdma = (dma_struct_p) info.pbd->context;
	assert (pdma != NULL);
	assert (info.ofs == 0);
	assert (info.len <= dif_req.rx_max_trans_len);

	assert (info.len <= dma_eff_buf_size (pdma));
	assert (info.len <= MAX_LENGTH);
	
	assert (q_get_count (rx_list) < dif_req.rx_block_num);
	res = q_enqueue_mem (rx_list, &info, sizeof (buf_info_t));
	assert (res != 0);
#ifdef DEBUG
	if (rx_last_buffer != NULL) {
		assert (rx_last_buffer->context == (void *) pdma);
	} else {
		assert (dif_req.tx_block_num > 0);
		assert (dif_req.tx_block_num < (2 * DMA_BUFFER_NUM_MAX));
		assert (pdma == dma_struct [dif_req.tx_block_num]);
	}
	rx_last_buffer = pdma;
	time = get_timer(desc->handle);
	last_to_pc_ack = time;
#endif
	pc_acknowledge(desc->handle, ndi_get_number(pdma));

	return 1;
}

asmlinkage unsigned OSHWCancelRx(hw_block_handle handle)
{
	assert (0);
	return 0;
}

asmlinkage unsigned OSHWCancelTx(hw_block_handle handle)
{
	assert (0);
	return 0;
}
