/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2002, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#ifndef _FCDSL2_QUEUE_H
#define _FCDSL2_QUEUE_H

#include <linux/skbuff.h>

typedef __u32 NCCI_t;

typedef struct __queue {
	struct sk_buff_head incoming;
	struct sk_buff_head parked;
	int pending;
	int limit;
} queue_t;

void queue_init(queue_t *q, int limit);
void queue_exit(queue_t *q);

int queue_put(queue_t *q, struct sk_buff *skb, bool charge);

static inline struct sk_buff *queue_get(queue_t *q)
{
	return skb_dequeue(&q->incoming);
}

static inline bool queue_is_empty(queue_t *q)
{
	return skb_queue_empty(&q->incoming);
}

void queue_park(queue_t *q, struct sk_buff *skb, unsigned appl, NCCI_t ncci,
		unsigned hand);
void queue_unpark_free(queue_t *q, unsigned appl, NCCI_t ncci, unsigned hand);

#endif /* !_FCDSL2_QUEUE_H */
