/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2002, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#include <linux/ioport.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/interrupt.h>
#include <linux/skbuff.h>
#include <linux/kernel.h>
#include <linux/spinlock.h>
#include <linux/pci.h>
#include <linux/delay.h>
#include <linux/ctype.h>
#include <linux/string.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/capi.h>
#include <linux/kernelcapi.h>
#include <linux/isdn/capicmd.h>
#include <linux/isdn/capiutil.h>
#include <linux/isdn/capilli.h>
#include "libstub.h"
#include "tables.h"
#include "queue.h"
#include "lib.h"
#include "tools.h"
#include "defs.h"
#include "fw.h"
#include "devif.h"

#define	KILOBYTE		1024
#define	MEGABYTE		(1024*KILOBYTE)
#define	_2_MEGS_		(2*MEGABYTE)
#define	_4_MEGS_		(4*MEGABYTE)
#define	_8_MEGS_		(8*MEGABYTE)
#define	TEN_MSECS		(HZ/100)

#define	MAPPED_DATA_LEN		_4_MEGS_
#define	MAPPED_CODE_LEN		_8_MEGS_
#define	IO_REGION_LEN		16
#define	DEBUG_MEM_SIZE		MEGABYTE
#define	DEBUG_TIMEOUT		100
#define	RESET_DELAY		10
#define DATA_REQ_QUEUE_LIMIT	16

#define	DBG_FORMAT_LEN		1024
#define	DBG_MEM_SIZE		(1024 * 1024)
#define	DBG_TIMEOUT		100
#define	DBG_MIN_SIZE		1024

typedef struct __db {
	dma_struct_p		pdma;
	unsigned		len;
	char *			pbuf;
	struct task_struct	*thread;
} dbg_buf_t, * dbg_buf_p;

struct fritz_card *capi_card;
DEFINE_MUTEX(stack_lock);

static int			nvers			= 0;
static char *			firmware_ptr		= NULL;
static unsigned			firmware_len		= 0;
static lib_callback_t *		capi_lib;

/*
 * More mess of the binary blob interface:
 *
 * link_library() expects to receive the address of a pointer that will once
 * point to an integer variable. That variable has to set to the logical
 * number of the second CAPI controller of the fcdsl2 (CA_KARTE() will access
 * is). In the past the variable was the first field of the card structure.
 * Now it is a dedicated int to avoid breaking things subtly when reordering
 * the struct content.
 *
 * Guys, never smoke that strange stuff again while trying to write code!
 */
static int ctr2_cnr;
static int *ctr2_cnr_ptr = &ctr2_cnr;

#ifdef DEBUG
static dbg_buf_p dbgbuf;
#endif

static asmlinkage void scheduler_control (unsigned);
static asmlinkage void wakeup_control (unsigned);
static asmlinkage void version_callback(char *vp);
static asmlinkage unsigned scheduler_suspend(unsigned long timeout);
static asmlinkage unsigned scheduler_resume(void);
static asmlinkage unsigned controller_remove(void);
static asmlinkage unsigned controller_add(void);

static functions_t ca_funcs = {
	7,
	scheduler_control,
	wakeup_control,
	version_callback,
	scheduler_suspend,
	scheduler_resume,
	controller_remove,
	controller_add
};

#ifdef __LP64__
# define _CAPIMSG_U64(m, off)	\
	 ((u64)m[off]|((u64)m[(off)+1]<<8)|((u64)m[(off)+2]<<16)|((u64)m[(off)+3]<<24) \
	 |((u64)m[(off)+4]<<32)|((u64)m[(off)+5]<<40)|((u64)m[(off)+6]<<48)|((u64)m[(off)+7]<<56))

static void _capimsg_setu64(void *m, int off, __u64 val)
{
	((__u8 *)m)[off] = val & 0xff;
	((__u8 *)m)[off+1] = (val >> 8) & 0xff;
	((__u8 *)m)[off+2] = (val >> 16) & 0xff;
	((__u8 *)m)[off+3] = (val >> 24) & 0xff;
	((__u8 *)m)[off+4] = (val >> 32) & 0xff;
	((__u8 *)m)[off+5] = (val >> 40) & 0xff;
	((__u8 *)m)[off+6] = (val >> 48) & 0xff;
	((__u8 *)m)[off+7] = (val >> 56) & 0xff;
}
#endif

static asmlinkage u8		minb(long addr) { return readb((void *)addr); }
static asmlinkage u16		minw(long addr) { return readw((void *)addr); }
static asmlinkage unsigned long	minl(long addr) { return readl((void *)addr); }
static asmlinkage u8		pinb(long addr) { return inb(addr); }
static asmlinkage u16		pinw(long addr) { return inb(addr); }
static asmlinkage unsigned long	pinl(long addr) { return inl(addr); }

static asmlinkage void moutb(u8 val, long addr)  { writeb(val, (void *)addr); }
static asmlinkage void moutw(u16 val, long addr) { writew(val, (void *)addr); }

static asmlinkage void moutl(unsigned long val, long addr)
{
	writel(val, (void *)addr);
}

static asmlinkage void poutb(u8 val, long addr)  { outb(val, addr); }
static asmlinkage void poutw(u16 val, long addr) { outw(val, addr); }

static asmlinkage void poutl(unsigned long val, long addr)
{
	outl(val, addr);
}

#define PCI_NO_RESOURCE		4
#define PCI_NO_PCI_KERN		3
#define PCI_NO_CARD		2
#define PCI_NO_PCI		1
#define PCI_OK			0

static int alloc_resources(struct pci_dev *dev, struct fritz_card *card)
{
	if (!request_region(card->addr_pio, IO_REGION_LEN, KBUILD_MODNAME)) {
		fcdsl2_err("Could not claim i/o region.\n");
	io_problem:
		card->pio_base = 0;
		return PCI_NO_RESOURCE;
	}
	card->pio_base = card->addr_pio;
	fritz_dbg("I/O region at 0x%04x\n", card->pio_base);
	card->data_base = ioremap_nocache(card->addr_data, card->len_data);
	if (!card->data_base) {
		fritz_dbg("Could not map data memory into virtual memory.\n");
	data_map_exit:
		release_region(card->addr_pio, IO_REGION_LEN);
		goto io_problem;
	}
	fritz_dbg("Controller data memory mapped to %p\n", card->data_base);
	card->code_base = ioremap_nocache(card->addr_code, card->len_code);
	if (!card->code_base) {
		fritz_dbg("Could not map code memory into virtual memory.\n");
	/* code_map_exit: */
		iounmap(card->data_base);
		card->data_base = NULL;
		goto data_map_exit;
	}
	fritz_dbg("Controller code memory mapped to %p\n", card->code_base);
	pci_set_master (dev);
	return PCI_OK;
}

static void free_resources(struct fritz_card *card)
{
	fritz_dbg("Unmapping code, data, i/o regions...\n");
	iounmap(card->code_base);
	iounmap(card->data_base);
	release_region(card->addr_pio, IO_REGION_LEN);
	fritz_dbg("Resources freed.\n");
}

#ifdef DEBUG

#define	DBG_FORMAT_START	0xABCD1234
#define	DBG_FORMAT_STOP		0x4321DCBA

void dump(char *p1, char *p2, char *end, unsigned l1, unsigned l2)
{
	p1 += sizeof (unsigned);
	p2 += sizeof (unsigned);
	memdump (p1, l1, p1 - p2, "");
	if (l2 != 0) {
		memdump (p2, l2, 0, "");
	}
}

static void dump_debug_buffer(dbg_buf_p dbp)
{
	static char *	bp = NULL;
	unsigned	part1_len, part2_len;
	unsigned	total_len, check_len;
	int		delta;
	
	assert (dbp != NULL);
	assert (dbp->pbuf != NULL);
	assert (dbp->pdma != NULL);
	assert (dbp->pbuf >= (char *) dbp->pdma->virt_addr);
	assert (dbp->pbuf < (char *) (dbp->pdma->virt_addr + dbp->len));
	assert ((((unsigned long) dbp->pbuf) & 3) == 0);
	if (bp == NULL) {
		bp = dbp->pbuf;
	}
	part1_len = part2_len = 0;
		
	assert (bp >= dbp->pbuf);
	assert (bp < (dbp->pbuf + dbp->len));
	total_len = * ((unsigned *) bp);
	info (total_len < dbp->len);
	if ((total_len > dbp->len) || ((total_len & 3) != 0)) {
		fritz_dbg("Debug thread needs debugging!\n");
		return;
	}

	delta = ((char *) bp + total_len) - (dbp->pbuf + dbp->len);
	if (delta > 0) {
		part2_len = (unsigned) delta;
		part1_len = total_len - part2_len;
	} else {
		part2_len = 0;
		part1_len = total_len;
	}
	assert ((part2_len & 3) == 0);
	assert ((part1_len & 3) == 0);
	assert (part2_len < dbp->len);

	if (part1_len != 0) {
		check_len = (part2_len == 0) 
			? * ((unsigned *) (bp + part1_len) + 1)
			: * ((unsigned *) (dbp->pbuf + part2_len) + 1);
		assert (check_len == total_len);
		
		dump (
			bp, 
			dbp->pbuf, 
			dbp->pbuf + dbp->len, 
			part1_len, 
			part2_len
		);

		if (part2_len == 0) {
			bp += 2 * sizeof (unsigned) + part1_len;
		} else {
			bp = dbp->pbuf + part2_len + sizeof (unsigned);
		}
	}
}

static int debug_thread(void *arg)
{
	dbg_buf_p dbp = arg;

	assert (dbp != NULL);
	while (!kthread_should_stop()) {
		msleep(DBG_TIMEOUT);
		dump_debug_buffer(dbp);
	}
	fritz_dbg("Debug thread stopped.\n");
	return 0;
}

static bool init_debug_buffer(dbg_buf_p dbp)
{
	bool res = false;
	unsigned	size	= DBG_MEM_SIZE;
	dma_struct_p	pdma;
	
	assert (dbp != NULL);
	assert (size >= DBG_MIN_SIZE);
	assert (DBG_TIMEOUT > 10);
	assert (dbp->pbuf == NULL);

	pdma = dma_alloc();
	for ( ; size >= DBG_MIN_SIZE; size /= 2) {
		if ((res = dma_setup (pdma, size))) {
			break;
		}
		fritz_dbg("Failed to allocate %u byte DMA buffer...\n", size);
	}
	if (!res) {
		dma_exit (pdma);
		dma_free (&pdma);
		return false;
	}
	fritz_dbg("Debug DMA buffer, %u bytes, vaddr %lx, paddr %lx\n",
		  pdma->length, pdma->virt_addr, pdma->phys_addr);
	assert (size == pdma->length);
	dbp->pdma = pdma;
	dbp->pbuf = (void *) pdma->virt_addr;
	dbp->len  = size;
	dbp->thread = kthread_run(debug_thread, dbp, KBUILD_MODNAME "-dbg");
	return true;
}

static void exit_debug_buffer(dbg_buf_p dbp)
{
	if (!dbp)
		return;

	fritz_dbg("Stopping debug thread...\n");
	kthread_stop(dbp->thread);
	if (dbp->pdma) {
		dma_exit(dbp->pdma);
		dma_free(&dbp->pdma);
	}
	kfree(dbp);
}

#else /* !DEBUG */

#define exit_debug_buffer(dbp)

#endif /* DEBUG */

static int stack_thread(void *arg)
{
	struct fritz_card *card = arg;
	bool rekick;

	fritz_dbg("Starting stack thread '%s'...\n", current->comm);

	while (!card->terminating) {
		mutex_lock(&stack_lock);
		spin_lock_irq(&card->kick_lock);

		if (!card->terminating && card->xfer_pending) {
			card->xfer_pending = false;
			spin_unlock_irq(&card->kick_lock);

			xfer_handler(card);

			spin_lock_irq(&card->kick_lock);
		}

		if (!card->terminating && card->scheduler_kicked) {
			card->scheduler_kicked = false;
			spin_unlock_irq(&card->kick_lock);

			os_timer_poll();
			rekick = (*capi_lib->cm_schedule)();

			spin_lock_irq(&card->kick_lock);
			card->scheduler_kicked |= rekick;
		}

		if (!card->terminating && !card->xfer_pending &&
		    !card->scheduler_kicked)
			set_current_state(TASK_INTERRUPTIBLE);

		spin_unlock_irq(&card->kick_lock);
		mutex_unlock(&stack_lock);

		schedule();
	}
	fritz_dbg("Scheduler thread stopped.\n");
	return 0;
}

static void reset(struct fritz_card *card)
{
	int done = 0;

	if (fw_ready ()) {
		fritz_dbg("Loadfile based reset.\n");
		done = fw_send_reset(card->c6205_ctx);
	}
	if (!done) {
		fritz_dbg("I/O based reset.\n");
		outl(C6205_PCI_HDCR_WARMRESET,
		     card->pio_base + C6205_PCI_HDCR_OFFSET);
	}
	mdelay(RESET_DELAY);
}

static void kill_version(struct fritz_ctrl *ctrl)
{
	kfree(ctrl->version);
	ctrl->version = NULL;
}

static inline void kill_versions(void)
{
	if (capi_card->ctrl[0])
		kill_version(capi_card->ctrl[0]);
	kill_version(capi_card->ctrl[1]);
}

static bool start(struct fritz_card *card)
{
	int		res;
	unsigned	num = 0, n;
	unsigned	ofs, len, bof;
	ioaddr_p	pwin;
	dma_struct_p *	ppdma;

	/* preparing the stack */
	table_init(&card->appls);
	queue_init(&card->queue, DATA_REQ_QUEUE_LIMIT);

	assert (capi_lib != NULL);
	assert (capi_lib->cm_register_ca_functions != NULL);
	assert (capi_lib->cm_start != NULL);
	assert (capi_lib->cm_init != NULL);
	(*capi_lib->cm_register_ca_functions) (&ca_funcs);
	if ((*capi_lib->cm_start) ()) {
		fcdsl2_err("Starting the stack failed...\n");
		return false;
	}
	(void) (*capi_lib->cm_init) (0, card->irq);
	assert (nvers == 2);
	
	/* install an interrupt handler */
	res = request_irq(card->irq, device_interrupt, IRQF_SHARED,
			  KBUILD_MODNAME, card);
	info (res == 0);
	if (0 != res) {
		fcdsl2_err("Could not install irq handler.\n");
		goto error_out1;
	}
	fritz_dbg("IRQ #%d assigned to " KBUILD_MODNAME " driver.\n",
		  card->irq);

#ifdef DEBUG
	/* debug buffer */
	assert (capi_lib->cc_debugging_needed != NULL);
	if ((*capi_lib->cc_debugging_needed) ()) {
		fritz_dbg("Setting up debug buffer.\n");
		if (dbgbuf == NULL) {
			dbgbuf = kzalloc(sizeof(dbg_buf_t), GFP_KERNEL);
			if (dbgbuf) {
				if (!init_debug_buffer (dbgbuf)) {
					kfree(dbgbuf);
					free_irq(card->irq, card);
					goto error_out1;
				}
				assert (capi_lib->cc_init_debug != NULL);
				(*capi_lib->cc_init_debug) (
					(void *) dbgbuf->pdma->phys_addr, 
					dbgbuf->pdma->length
				);
			}
		}
	} else {
		fritz_dbg("No debug buffer.\n");
	}
#endif /* DEBUG */

	/* establish DMA buffers */
	ppdma = dma_get_struct_list (&num);
	assert (capi_lib->cc_num_link_buffer != NULL);
	assert (capi_lib->cc_init_dma != NULL);
	n = (*capi_lib->cc_num_link_buffer)(DMA_TX),
	(*capi_lib->cc_init_dma)(&ppdma[0], n, &ppdma[n],
				 (*capi_lib->cc_num_link_buffer)(DMA_RX));
	
	assert (capi_lib->cc_link_version != NULL);
	assert (capi_lib->cc_buffer_params != NULL);
	if ((*capi_lib->cc_link_version) () > 1) {
		num = (*capi_lib->cc_buffer_params) (&ofs, &pwin, &len, &bof);
		dif_set_params (num, ofs, pwin, len, bof);
	}
	reset(card);
	assert (capi_lib->cc_run != NULL);
	res = (*capi_lib->cc_run)();
	if (res == 0) {
		fcdsl2_err("Firmware does not respond!\n");
		goto error_out2;
	}
	assert (capi_lib->cc_compress_code != NULL);
	(*capi_lib->cc_compress_code)();

	/* start the stack */
	if ((*capi_lib->cm_activate)()) {
		fcdsl2_err("Activation of the card failed.\n");
		goto error_out2;
	}
	card->running = true;
	return true;

error_out2:
	reset(card);
	exit_debug_buffer(dbgbuf);
	free_irq(card->irq, card);

error_out1:
	assert (capi_lib->cm_exit != NULL);
	(*capi_lib->cm_exit)();
	kill_versions();
	queue_exit(&card->queue);
	table_exit(&card->appls);
	return false;
}

static void stop(struct fritz_card *card)
{
	if (!card->running) {
		fritz_dbg("Stack not initialized.\n");
		return;
	}
	exit_debug_buffer(dbgbuf);
	(*capi_lib->cm_exit)();
	card->running = false;
	free_irq(card->irq, card);

	queue_exit (&card->queue);
	table_exit (&card->appls);
	kill_versions ();
	nvers = 0;
}

static void reset_card(struct fritz_card *card)
{
	reset(card);
	fw_exit(&card->c6205_ctx);
}

static int prepare_card(struct fritz_card *card, struct pci_dev *dev)
{
	assert (pci_resource_len (dev, 0) == MAPPED_DATA_LEN);
	assert (pci_resource_len (dev, 1) == MAPPED_CODE_LEN);
	assert (pci_resource_len (dev, 2) == IO_REGION_LEN);

	card->addr_data = pci_resource_start (dev, 0);
	card->len_data  = pci_resource_len (dev, 0);
	card->addr_code = pci_resource_start (dev, 1);
	card->len_code  = pci_resource_len (dev, 1);
	card->addr_pio  = pci_resource_start (dev, 2);
	card->len_pio   = pci_resource_len (dev, 2);
	card->irq       = dev->irq;

	card->stack_thread = ERR_PTR(-ENODEV);
	card->xfer_pending = false;
	card->scheduler_kicked = false;
	spin_lock_init(&card->kick_lock);

	fritz_dbg("PCI: " PRODUCT_LOGO ", dev %04x, irq %d\n",
		  dev->device, card->irq);
	fritz_dbg("     data %lx (%luM), code %lx (%luM), pio %04lx\n",
		  card->addr_data, card->len_data / MEGABYTE,
		  card->addr_code, card->len_code / MEGABYTE,
		  card->addr_pio);

	return alloc_resources(dev, card);
}

static struct capi_ctr *get_capi_ctr(int nctr)
{
	struct capi_ctr *ctr;
	int i;

	for (i = 0; i < ARRAY_SIZE(capi_card->ctrl); i++) {
		ctr = &capi_card->ctrl[i]->ctr;
		if (ctr->cnr == nctr)
			return ctr;
	}
	return NULL;
}

int nbchans(struct capi_ctr *ctr)
{
	struct fritz_ctrl *ctrl = capi2fritz_ctrl(ctr);
	unsigned char *prf;
	int temp = 2;

	prf = (unsigned char *)ctrl->string[6];
	if (prf != NULL) {
		temp = prf[2] + 256 * prf[3];
	}
	return temp;
}

static void scan_version(struct capi_ctr *ctr, const char *ver)
{
	struct fritz_ctrl *ctrl = capi2fritz_ctrl(ctr);
	int vlen, i;
	char *vstr;

	vlen = (unsigned char) ver[0];
	ctrl->version = vstr = kmalloc(vlen, GFP_KERNEL);
	if (!vstr) {
		fritz_dbg("Could not allocate version buffer.\n");
		return;
	}
	memcpy(ctrl->version, ver + 1, vlen);
	i = 0;
	for (i = 0; i < 8; i++) {
		ctrl->string[i] = vstr + 1;
		vstr += 1 + *vstr;
	}
	fritz_dbg("Library version:    %s\n", ctrl->string[0]);
	fritz_dbg("Card type:          %s\n", ctrl->string[1]);
	fritz_dbg("Capabilities:       %s\n", ctrl->string[4]);
	fritz_dbg("D-channel protocol: %s\n", ctrl->string[5]);
}

static void copy_version(struct capi_ctr *ctr)
{
	struct fritz_ctrl *ctrl = capi2fritz_ctrl(ctr);
	char *tmp;

	tmp = ctrl->string[3];
	if (!tmp) {
		fcdsl2_err("Do not have version information...\n");
		return;
	}
	strncpy(ctr->serial, tmp, CAPI_SERIAL_LEN);
#if !defined (DRIVER_TYPE_ISDN)
	if (ctrl == capi_controller[0])
		memset(&ctr->profile, 0, sizeof(capi_profile));
	else
		memcpy(&ctr->profile, ctrl->string[6], sizeof(capi_profile));
#else
	memcpy(&ctr->profile, ctrl->string[6], sizeof(capi_profile));
#endif
	strncpy(ctr->manu, "AVM-GmbH", CAPI_MANUFACTURER_LEN);
	ctr->version.majorversion = 2;
	ctr->version.minorversion = 0;
	tmp = ctrl->string[0];
	ctr->version.majormanuversion = (((tmp[0] - '0') & 15) << 4)
					+ ((tmp[2] - '0') & 15);
	ctr->version.minormanuversion = ((tmp[3] - '0') << 4)
					+ (tmp[5] - '0') * 10
					+ ((tmp[6] - '0') & 15);
}

#ifdef DEBUG
unsigned get_timer(struct fritz_card *card)
{
	return readl(card->code_base + card->timer_offset);
}
#endif

void pc_acknowledge(struct fritz_card *card, unsigned char num)
{
#ifdef LOG_LINK
	fritz_dbg("ACK for buffer #%u\n", num);
#endif
	writel(num, card->data_base + card->pc_ack_offset);
}

int load_ware(struct capi_ctr *ctr, capiloaddata *ware)
{
	struct fritz_card *card;
	c6205_context ctx;
	char *buffer;
	int err;

	card = capi2fritz_card(ctr);
	if (card->running) {
		fritz_dbg("Firmware has already been loaded!\n");
		return 0;
	}
	assert (firmware_ptr == NULL);
	buffer = vmalloc(ware->firmware.len);
	if (!buffer) {
		fcdsl2_err("Could not allocate firmware buffer.\n");
		return -ENOMEM;
	}
        if (ware->firmware.user) {
		if (copy_from_user(buffer, ware->firmware.data,
				   ware->firmware.len)) {
			fcdsl2_err("Error copying firmware data!\n");
			vfree(buffer);
			return -EIO;
		}
	} else {
		memcpy(buffer, ware->firmware.data, ware->firmware.len);
	}
	fritz_dbg("Loaded %d bytes of firmware.\n", ware->firmware.len);
	firmware_ptr = buffer;
	firmware_len = ware->firmware.len;

#define	INIT_IO(x,n,f)	x.virt_addr	= (long)card->n##_base;	\
			x.phys_addr	= card->addr_##n;		\
			x.length	= card->len_##n;		\
			x.inb		= &(f##inb);		\
			x.inw		= &(f##inw);		\
			x.inl		= &(f##inl);		\
			x.outb		= &(f##outb);		\
			x.outw		= &(f##outw);		\
			x.outl		= &(f##outl);
	INIT_IO(card->iowin[0], data, m);
	INIT_IO(card->iowin[1], code, m);
	INIT_IO(card->iowin[2], pio, p);
#undef INIT_IO

	mutex_lock(&stack_lock);

	ctx = fw_init(NULL, firmware_ptr, firmware_len, 3, card->iowin, &err);
	if ((NULL == ctx) || (err != 0)) {
		fcdsl2_err("Error while processing firmware (%d)!\n", err);
		err = -EIO;
		goto error_out_1;
	}
	card->c6205_ctx = ctx;
	assert (capi_lib != NULL);
	assert (capi_lib->cc_link_version != NULL);
	assert ((*capi_lib->cc_link_version) () > 1);

	if ((*capi_lib->cc_link_version)() < 2 || !dif_init()) {
		fcdsl2_err("Error while setting up device structures!\n");
		err = -EIO;
		goto error_out_1;
	}
	assert (capi_lib->cc_timer_offset != NULL);
	card->timer_offset = (*capi_lib->cc_timer_offset)();
	if ((*capi_lib->cc_link_version) () > 3) {
		card->pc_ack_offset = (*capi_lib->cc_pc_ack_offset)();
	}

	fw_setup (ctx);
	if (!start(card)) {
		fcdsl2_err("Error while starting protocoll stack!\n");
		err = -EIO;
		goto error_out_2;
	}

	card->terminating = false;
	card->stack_thread = kthread_run(stack_thread, card, KBUILD_MODNAME);
	if (IS_ERR(card->stack_thread)) {
		fcdsl2_err("Unable to start stack thread!\n");
		err = PTR_ERR(card->stack_thread);
		goto error_out_3;
	}

	mutex_unlock(&stack_lock);

	if (card->ctrl[0])
		capi_ctr_ready(&card->ctrl[0]->ctr);
	capi_ctr_ready(&card->ctrl[1]->ctr);
	return 0;

error_out_3:
	stop(card);

error_out_2:
	dif_exit();

error_out_1:
	fw_exit(&ctx);
	mutex_unlock(&stack_lock);
	vfree(firmware_ptr);
	firmware_ptr = NULL;

	return err;
}

char *proc_info(struct capi_ctr *ctr)
{
	struct fritz_ctrl *ctrl = capi2fritz_ctrl(ctr);
	struct fritz_card *card = ctrl->card;
	static char text[80];

	if (card->running)
		snprintf(text, sizeof(text), "%s %s io %lx %u",
			 ctrl->version ? ctrl->string[1] : "A1",
			 ctrl->version ? ctrl->string[0] : "-",
			 card->addr_pio, card->irq);
	else
		snprintf(text, sizeof(text),
			 KBUILD_MODNAME " device io %lx irq %u",
			 card->addr_pio, card->irq);
	return text;
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,34)
int ctr_info(struct seq_file *m, void *v)
{
	struct capi_ctr *ctr = m->private;
	struct fritz_ctrl *ctrl = capi2fritz_ctrl(ctr);
	struct fritz_card *card = ctrl->card;
	unsigned char flag;
	char *temp;

	seq_printf(m, "%-16s %s\n", "name", SHORT_LOGO);
	seq_printf(m, "%-16s %d\n", "irq", card->irq);
	seq_printf(m, "%-16s %p\n", "data", card->data_base);
	seq_printf(m, "%-16s %p\n", "code", card->code_base);
	seq_printf(m, "%-16s %x\n", "io", card->pio_base);

	if (card->running) {
		temp = ctrl->version ? ctrl->string[1] : "A1";
		seq_printf(m, "%-16s %s\n", "type", temp);

		temp = ctrl->version ? ctrl->string[0] : "-";
		seq_printf(m, "%-16s %s\n", "ver_driver", temp);
		seq_printf(m, "%-16s %s\n", "ver_cardtype", SHORT_LOGO);

		flag = ((unsigned char *)(ctr->profile.manu))[3];
		if (flag)
			seq_printf(m, "%-16s%s%s%s%s%s%s%s\n", "protocol",
				   (flag & 0x01) ? " DSS1" : "",
				   (flag & 0x02) ? " CT1" : "",
				   (flag & 0x04) ? " VN3" : "",
				   (flag & 0x08) ? " NI1" : "",
				   (flag & 0x10) ? " AUSTEL" : "",
				   (flag & 0x20) ? " ESS" : "",
				   (flag & 0x40) ? " 1TR6" : "");

		flag = ((unsigned char *)(ctr->profile.manu))[5];
		if (flag)
			seq_printf(m, "%-16s%s%s%s%s\n", "linetype",
				   (flag & 0x01) ? " point to point" : "",
				   (flag & 0x02) ? " point to multipoint" : "",
				   (flag & 0x08) ?
					" leased line without D-channel" : "",
				   (flag & 0x04) ?
					" leased line with D-channel" : "");
	}
	return 0;
}

static int ctr_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, ctr_info, PDE(inode)->data);
}

static const struct file_operations proc_fops = {
	.owner		= THIS_MODULE,
	.open		= ctr_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};
#endif /* < 2.6.34 */

static void reset_ctrl(struct capi_ctr *ctr)
{
	struct task_struct *thread;
	struct fritz_card *card;

	card = capi2fritz_card(ctr);

	if (card->ctrl[0])
		capi_ctr_down(&card->ctrl[0]->ctr);
	capi_ctr_down(&card->ctrl[1]->ctr);

	mutex_lock(&stack_lock);

	thread = card->stack_thread;
	if (!IS_ERR(thread)) {
		spin_lock_irq(&card->kick_lock);
		card->stack_thread = ERR_PTR(-ENODEV);
		card->terminating = true;
		spin_unlock_irq(&card->kick_lock);
		/*
		 * Drop the mutex while stopping the thread as it may wait for
		 * it ATM.
		 */
		mutex_unlock(&stack_lock);
		kthread_stop(thread);
		mutex_lock(&stack_lock);
	}

	stop(card);
	reset_card(card);
	vfree(firmware_ptr);
	firmware_ptr = NULL;
	firmware_len = 0;
	dif_exit ();

	mutex_unlock(&stack_lock);
}

static int setup_ctrl(struct fritz_card *card, int ix)
{
	struct fritz_ctrl *ctrl;
	struct capi_ctr *ctr;
	int err;

	ctrl = kzalloc(sizeof(*ctrl), GFP_KERNEL);
	if (!ctrl)
		return -ENOMEM;

	ctr = &ctrl->ctr;
	ctr->driver_name	= KBUILD_MODNAME;
	ctr->driverdata		= ctr;
	ctr->owner		= THIS_MODULE;
	ctr->load_firmware	= load_ware;
	ctr->reset_ctr		= reset_ctrl;
	ctr->register_appl	= register_appl;
	ctr->release_appl	= release_appl;
	ctr->send_message	= send_msg;
	ctr->procinfo		= proc_info;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,34)
	ctr->proc_fops		= &proc_fops;
#endif
	snprintf(ctr->name, 32, "%s-%lx-%u", KBUILD_MODNAME, card->addr_pio,
		 card->irq);

	err = attach_capi_ctr(ctr);
	if (err) {
		fcdsl2_err("Error: Could not attach controller %d.\n", ix);
		return -EBUSY;
	}
	fritz_dbg("Controller #%d (%d)\n", ix+1, ctr->cnr);
	ctrl->card = card;
	card->ctrl[ix] = ctrl;
	return 0;
}

int add_card(struct pci_dev *dev)
{
	struct fritz_card *card;
	int res = 0;
	char *msg;

	if (NULL != capi_card) {
		fcdsl2_err("Cannot handle two controllers!\n");
 		return -EBUSY;
	}
	card = kzalloc(sizeof(*card), GFP_KERNEL);
	if (!card) {
		fcdsl2_err("Card object allocation failed.\n");
		return -EIO;
	}
	capi_card = card;

	switch (prepare_card(card, dev)) {
	case PCI_OK:
		break;
	case PCI_NO_RESOURCE:
		msg = "Could not claim required resources.";
		res = -EBUSY;
		break;
        case PCI_NO_PCI_KERN:
		msg = "No PCI kernel support available.";
		res = -ESRCH;
		break;
        case PCI_NO_CARD:
		msg = "Could not locate any " PRODUCT_LOGO ".";
		res = -ESRCH;
		break;
        case PCI_NO_PCI:
		msg = "No PCI busses available.";
		res = -ESRCH;
		break;
        default:
		msg = "Unknown PCI related problem...";
		res = -EINVAL;
		break;
	}
	if (res != 0) {
		fcdsl2_err("Error: %s\n", msg);
		kfree(card);
		capi_card = NULL;
		return res;
	}
#if !defined (DRIVER_TYPE_ISDN)
	res = setup_ctrl(card, 1);
#else
	res = setup_ctrl(card, 0);

	if (res == 0) {
		res = setup_ctrl(card, 1);
		if (res != 0) {
			detach_capi_ctr(&card->ctrl[0]->ctr);
			kfree(card->ctrl[0]);
		}
	}
#endif
	if (res == 0) {
		/* See comment on definition of ctr2_cnr */
		ctr2_cnr = card->ctrl[1]->ctr.cnr;
	} else {
		free_resources(card);
		kfree(card);
	}
	return res;
}

void remove_ctrls(struct fritz_card *card)
{
	info (!card->running);
	if (card->running) {
		fcdsl2_err("Remove controller w/o reset!\n");
		reset_ctrl(&card->ctrl[1]->ctr);
	}
	if (card->ctrl[0])
		detach_capi_ctr(&card->ctrl[0]->ctr);
	detach_capi_ctr(&card->ctrl[1]->ctr);
	free_resources(card);
	kfree(card->ctrl[0]);
	kfree(card->ctrl[1]);
	kfree(card);
	capi_card = NULL;
}

int msg2stack(unsigned char *msg)
{
	unsigned mlen, appl, hand, nctr;
	unsigned char *mptr;
	struct sk_buff *skb;
	__u32 ncci;
	
	assert (capi_card != NULL);
	assert (msg != NULL);

	skb = queue_get(&capi_card->queue);
	if (!skb)
		return 0;

	mptr = (unsigned char *) skb->data;
	mlen = CAPIMSG_LEN(mptr); 
	appl = CAPIMSG_APPID(mptr);
	nctr = CAPIMSG_CONTROLLER(mptr);

	MLOG("PUT_MESSAGE(ctrl:%u,appl:%u,cmd:%X,subcmd:%X,len:%u)\n",
	     nctr, appl, CAPIMSG_COMMAND(mptr), CAPIMSG_SUBCOMMAND(mptr),
	     mlen);

	if (CAPIMSG_CMD(mptr) == CAPI_DATA_B3_REQ) {
		hand = CAPIMSG_U16(mptr, 18);
		ncci = CAPIMSG_NCCI(mptr);
#ifdef __LP64__
		if (mlen < 30) {
			_capimsg_setu64(msg, 22, (__u64)(mptr + mlen));
			capimsg_setu16(mptr, 0, 30);
		} else {
			_capimsg_setu64(mptr, 22, (__u64)(mptr + mlen));
		}
#else
		capimsg_setu32(mptr, 12, (__u32)(mptr + mlen));
#endif
		memcpy(msg, mptr, mlen);
		queue_park(&capi_card->queue, skb, appl, ncci, hand);
	} else {
		memcpy(msg, mptr, mlen);
		assert (skb->next == NULL);
		kfree_skb (skb);
	}
	return 1;
}

void msg2capi(unsigned char *msg)
{
	unsigned mlen, appl, dlen, nctr;
	struct capi_ctr *ctr;
	unsigned char *dptr;
	struct sk_buff *skb;
#ifndef __LP64__
	__u32 dummy;
#endif

	assert (capi_card != NULL);
	assert (msg != NULL);
	mlen = CAPIMSG_LEN(msg);
	appl = CAPIMSG_APPID(msg);
	nctr = CAPIMSG_CONTROLLER(msg);
	
	MLOG("GET_MESSAGE(ctrl:%u,appl:%d,cmd:%X,subcmd:%X,len:%u)\n",
	     nctr, appl, CAPIMSG_COMMAND(msg), CAPIMSG_SUBCOMMAND(msg), mlen);

	if (CAPIMSG_CMD(msg) == CAPI_DATA_B3_CONF)
		handle_data_conf(&capi_card->appls, &capi_card->queue, msg);

	if (!appl_alive(&capi_card->appls, appl)) {
		fritz_dbg("Message to dying appl #%u!\n", appl);
		return;
	}
	if (CAPIMSG_CMD(msg) == CAPI_DATA_B3_IND) {
		dlen = CAPIMSG_DATALEN(msg);

		skb = alloc_skb(mlen + dlen + ((mlen < 30) ? (30 - mlen) : 0),
				GFP_KERNEL);
		if (!skb) {
			fcdsl2_err("Unable to allocate skb. Message lost.\n");
			return;
		}
		/* Messages are expected to come with 32 bit data pointers. 
		 * The kernel CAPI works with extended (64 bit ready) message 
		 * formats so that the incoming message needs to be fixed, 
		 * i.e. the length gets adjusted and the required 64 bit data 
		 * pointer is added.
		 */
#ifdef __LP64__
		dptr = (unsigned char *) _CAPIMSG_U64(msg, 22);
		memcpy(skb_put(skb, mlen), msg, mlen);
#else
		dptr = (unsigned char *) CAPIMSG_U32(msg, 12);
		if (mlen < 30) {
			msg[0] = 30;
			dummy  = 0;	
			memcpy(skb_put(skb, mlen), msg, mlen);
			memcpy(skb_put(skb, 4), &dummy, 4);
			memcpy(skb_put(skb, 4), &dummy, 4);
		} else {
			memcpy(skb_put(skb, mlen), msg, mlen);
		}
#endif
		memcpy(skb_put(skb, dlen), dptr, dlen);
	} else {
		skb = alloc_skb(mlen, GFP_KERNEL);
		if (!skb) {
			fcdsl2_err("Unable to allocate skb. Message lost.\n");
			return;
		}
		memcpy(skb_put(skb, mlen), msg, mlen);
	}
	ctr = get_capi_ctr(nctr);
	if (!ctr) {
		ctr = (capi_card->ctrl[0])
			? &capi_card->ctrl[0]->ctr
			: &capi_card->ctrl[1]->ctr;
	}
	capi_ctr_handle_message(ctr, appl, skb);
}

static asmlinkage void wakeup_control(unsigned enable_flag)
{
	enable_flag = 0;
}

static asmlinkage void version_callback(char *vp)
{
	struct capi_ctr *ctr;

	if (nvers >= 2 || (nvers == 0 && !capi_card->ctrl[0])) {
		fritz_dbg("Version string #%d not available.\n", nvers);
	} else {
		fritz_dbg("Version string #%d:\n", nvers);
		ctr = &capi_card->ctrl[nvers]->ctr;
		scan_version(ctr, vp);
		copy_version(ctr);
	}
	++nvers;
}

static asmlinkage unsigned scheduler_suspend(unsigned long timeout)
{
	return 0;
}

static asmlinkage unsigned scheduler_resume(void)
{
	return 0;
}

static asmlinkage unsigned controller_remove(void)
{
	assert (0);
	return 0;
}

static asmlinkage unsigned controller_add(void)
{
	assert (0);
	return 0;
}

void kick_stack_thread(struct fritz_card *card, bool kick_xfer,
		       bool kick_scheduler)
{
	unsigned long flags;

	spin_lock_irqsave(&card->kick_lock, flags);

	card->xfer_pending |= kick_xfer;
	card->scheduler_kicked |= kick_scheduler;

	if (!IS_ERR(card->stack_thread))
		wake_up_process(card->stack_thread);

	spin_unlock_irqrestore(&card->kick_lock, flags);
}

static asmlinkage void scheduler_control(unsigned kicked)
{
	struct fritz_card *card = capi_card;

	spin_lock_irq(&card->kick_lock);

	card->scheduler_kicked = kicked;
	if (kicked)
		wake_up_process(card->stack_thread);

	spin_unlock_irq(&card->kick_lock);
}

void init(unsigned len, void (asmlinkage * reg) (void *, unsigned),
	  void (asmlinkage * rel) (void *), void (asmlinkage * dwn) (void))
{
	assert (reg != NULL);
	assert (rel != NULL);
	assert (dwn != NULL);

	capi_card->length   = len;
	capi_card->reg_func = reg;
	capi_card->rel_func = rel;
	capi_card->dwn_func = dwn;
}

int __devinit fcdsl2_driver_init(void)
{
	capi_lib = link_library(&ctr2_cnr_ptr);
	return (capi_lib != NULL);
}

void fcdsl2_driver_exit(void)
{
	assert (capi_lib);
	free_library ();
	capi_lib = NULL;
}
