/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2002, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#include "tools.h"
#include "queue.h"

struct parked_skb_key {
	unsigned appl;
	NCCI_t ncci;
	unsigned hand;
};

void queue_init(queue_t *q, int limit)
{
	skb_queue_head_init(&q->incoming);
	skb_queue_head_init(&q->parked);
	q->pending = 0;
	q->limit = limit;
}

void queue_exit(queue_t *q)
{
	skb_queue_purge(&q->incoming);
	skb_queue_purge(&q->parked);
}

int queue_put(queue_t *q, struct sk_buff *skb, bool charge)
{
	unsigned long flags;
	int res = 0;

	spin_lock_irqsave(&q->parked.lock, flags);

	if (!charge || q->pending < q->limit) {
		__skb_queue_tail(&q->incoming, skb);
		if (charge)
			q->pending++;
	} else
		res = -EBUSY;

	spin_unlock_irqrestore(&q->parked.lock, flags);

	return res;
}

void queue_park(queue_t *q, struct sk_buff *skb, unsigned appl, NCCI_t ncci,
		unsigned hand)
{
	struct parked_skb_key *key = (struct parked_skb_key *)skb->cb;

	key->appl = appl;
	key->ncci = ncci;
	key->hand = hand;

	skb_queue_tail(&q->parked, skb);
}

void queue_unpark_free(queue_t *q, unsigned appl, NCCI_t ncci, unsigned hand)
{
	struct sk_buff *match = NULL;
	struct parked_skb_key *key;
	struct sk_buff *skb;
	unsigned long flags;

	spin_lock_irqsave(&q->parked.lock, flags);

	skb_queue_walk(&q->parked, skb) {
		key = (struct parked_skb_key *)skb->cb;
		if (key->appl == appl && key->ncci == ncci &&
		    key->hand == hand) {
			match = skb;
			__skb_unlink(match, &q->parked);
			q->pending--;
			break;
		}
	}

	spin_unlock_irqrestore(&q->parked.lock, flags);

	if (!match)
		fritz_dbg("Tried to confirm unknown data b3 message.\n");

	kfree_skb(match);
}
