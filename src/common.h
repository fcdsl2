/*
 * AVM * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2003, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#ifndef __have_common_h__
#define __have_common_h__

struct __ndi;
struct c6205_ctx;

typedef void *(asmlinkage * code_func_t) (void *, unsigned, unsigned *);
typedef void (asmlinkage * wait_func_t) (unsigned);
typedef unsigned (asmlinkage * xfer_func_t) (void *, unsigned);

enum dma_dir { DMA_RX = 0, DMA_TX = 1};

typedef struct __dst {
	enum dma_dir		direction;
	unsigned		length;
	void *			alloc_ptr;
	long			phys_addr;
	long			virt_addr;
	void *			context;
} dma_struct_t, * dma_struct_p;

typedef dma_struct_p *		dma_list_t;

typedef struct __ndi		num_dma_info_t, * num_dma_info_p;

typedef struct __ist {
	unsigned		length;
	long			phys_addr;
	long			virt_addr;

	u8		(asmlinkage * inb) (long);
	u16		(asmlinkage * inw) (long);
	unsigned long	(asmlinkage * inl) (long);
	void		(asmlinkage * outb) (u8, long);
	void		(asmlinkage * outw) (u16, long);
	void		(asmlinkage * outl) (unsigned long, long);
} ioaddr_t, * ioaddr_p;

typedef struct c6205_ctx *	c6205_context;

typedef struct __dreq {
	unsigned		tx_block_num;
	unsigned		tx_block_size;
	unsigned		tx_max_trans_len;
	unsigned		rx_block_num;
	unsigned		rx_block_size;
	unsigned		rx_max_trans_len;
	code_func_t		get_code;
	void *			context;
	wait_func_t		stack_wait;
} dif_require_t, * dif_require_p;

typedef struct __ireq {
	xfer_func_t	tx_func;
} in_require_t, * in_require_p;

typedef void *hw_block_handle;

typedef struct __buf_desc {
	hw_block_handle			handle;		/* const */
	unsigned char *			buffer;		/* const */
	unsigned long			length;		/* const */
	void *				context;	/* const */
} hw_buffer_descriptor_t, * hw_buffer_descriptor_p;

typedef void (asmlinkage *
	      hw_completion_func_t) (const hw_buffer_descriptor_p desc,
				     unsigned offset, unsigned length,
				     void *context);

typedef void (asmlinkage * hw_event_func_t) (unsigned event_mask,
					     const hw_buffer_descriptor_p desc,
					     unsigned offset,
					     unsigned length, void *context);

#endif
