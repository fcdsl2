/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2002, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#ifndef __have_tools_h__
#define __have_tools_h__

#include <linux/types.h>
#include <linux/spinlock.h>
#include "defs.h"

#if defined (DRIVER_TYPE_USB)
#if defined (info)
#undef info
#endif
#endif

#ifdef DEBUG

#define assert(x) do { \
	if (!(x)) \
		fritz_dbg("%s(%d): assert (%s) failed\n", \
			  __FILE__, __LINE__, #x); \
} while (0)

#define info(x) do { \
	if (!(x)) \
		fritz_dbg("%s(%d): info (%s) failed\n", \
			  __FILE__, __LINE__, #x); \
} while (0)

#define fritz_dbg(f, ...) \
	printk(KERN_DEBUG KBUILD_MODNAME ": " f, ##__VA_ARGS__)

#else /* !DEBUG */
#define assert(x)
#define info(x)
#define fritz_dbg(f, ...) ({ \
	if (0) printk(KERN_DEBUG KBUILD_MODNAME ": " f, ##__VA_ARGS__); \
	0; \
})
#endif /* !DEBUG */

#ifdef LOG_MESSAGES
#define MLOG fritz_dbg
#else
#define MLOG(f, a...)
#endif

#define fcdsl2_err(f, ...) \
	printk(KERN_ERR KBUILD_MODNAME ": " f, ##__VA_ARGS__)

typedef struct __q ptr_queue_t, *ptr_queue_p;

ptr_queue_p q_make(unsigned max);
void q_remove(ptr_queue_p *qpp);
void q_reset(ptr_queue_p qp);

bool q_attach_mem(ptr_queue_p qp, unsigned n, unsigned len);

bool q_enqueue(ptr_queue_p qp, void *p);
bool q_enqueue_mem(ptr_queue_p qp, void *m, unsigned len);

bool q_dequeue(ptr_queue_p qp, void **pp);
bool q_peek(ptr_queue_p qp, void **pp);

unsigned q_get_count(ptr_queue_p qp);

void memdump(const void *mem, unsigned len, unsigned start, const char *msg);

#endif
