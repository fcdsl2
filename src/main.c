/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2002, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#include <linux/pci.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/string.h>
#include <linux/capi.h>
#include <linux/ctype.h>
#include <linux/isdn/capilli.h>
#include "driver.h" 
#include "lib.h"
#include "tools.h"
#include "defs.h"

static struct pci_device_id fcdsl2_id_table[] = {
	{ PCI_VENDOR_ID_AVM, PCI_DEVICE_ID_FCDSL2,
	  PCI_ANY_ID, PCI_ANY_ID, 0, 0, 0 },
	{ /* Terminating entry */ }
};

MODULE_DEVICE_TABLE (pci, fcdsl2_id_table);

#ifdef CONFIG_LOCKDEP
MODULE_LICENSE("GPL"); /* A BIG LIE! But lockdep doesn't work otherwise. */
#else
MODULE_LICENSE("Proprietary");
#endif

MODULE_DESCRIPTION("CAPI4Linux: Driver for " PRODUCT_LOGO);

static int __devinit fcdsl2_probe(struct pci_dev *dev,
				  const struct pci_device_id *id)
{
	int res;

	if (capi_card != NULL) {
		fcdsl2_err("Sorry, this poor driver only supports one PCI "
			   "card\n");
		return -EBUSY;
	}
	if (pci_enable_device (dev) < 0) {
		fcdsl2_err("Error: Failed to enable " PRODUCT_LOGO "!\n");
		return -ENODEV;
	}
	fritz_dbg("Loading...\n");
	if (!fcdsl2_driver_init()) {
		fcdsl2_err("Error: Driver library not available.\n");
		return -ENOSYS;
	}
	if (0 != (res = add_card (dev))) {
		fritz_dbg("Not loaded.\n");
		fcdsl2_driver_exit();
		return res;
	}
	fritz_dbg("Loaded.\n");
	assert (capi_card != NULL);
	pci_set_drvdata (dev, capi_card);
	
	return 0;
}

static void __devexit fcdsl2_remove(struct pci_dev *dev)
{
	struct fritz_card *card;

	card = pci_get_drvdata(dev);

	fritz_dbg("Removing...\n");
	remove_ctrls(card);
	fritz_dbg("Removed.\n");
	fcdsl2_driver_exit();
}

static struct pci_driver fcdsl2_driver = {
	.name		= KBUILD_MODNAME,
	.id_table	= fcdsl2_id_table,
	.probe		= fcdsl2_probe,
	.remove		= __devexit_p(fcdsl2_remove),
};

static struct capi_driver fcdsl2_capi_driver = {
	.name		= KBUILD_MODNAME,
	.revision	= DRIVER_REV,
};

static int __init fcdsl2_init(void)
{
	int err;

	printk(KERN_INFO "%s, revision %s\n", DRIVER_LOGO, DRIVER_REV);

	err = pci_register_driver(&fcdsl2_driver);
	if (err)
		return err;
	fritz_dbg("PCI driver registered.\n");

	register_capi_driver (&fcdsl2_capi_driver);
	fritz_dbg("CAPI driver registered.\n");

	return 0;
}

static void __exit fcdsl2_exit(void)
{
	unregister_capi_driver (&fcdsl2_capi_driver);
	fritz_dbg("CAPI driver unregistered.\n");
	pci_unregister_driver (&fcdsl2_driver);	
	fritz_dbg("PCI driver unregistered.\n");
}

module_init(fcdsl2_init);
module_exit(fcdsl2_exit);
