/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2002, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#ifndef __have_tables_h__
#define __have_tables_h__

#include <linux/skbuff.h>
#include <linux/list.h>
#include <linux/capi.h>
#include "queue.h"

typedef u16	capiinfo_t;

typedef struct __ncci {		/* tables.c only */
	NCCI_t			ncci;
	unsigned		appl;
	unsigned		win_size;
	unsigned		blk_size;
	unsigned char **	data;
	struct __ncci *		pred;
	struct __ncci *		succ;
} ncci_t;

typedef struct __appl {
	unsigned		id;
	bool			dying;
	void *			data;
	unsigned		attr;
	unsigned		blk_size;
	unsigned		blk_count;
	unsigned		ncci_count;
	unsigned		nncci;
	ncci_t *		root;
	struct __appl *		pred;
	struct __appl *		succ;
} appl_t;

typedef struct __appltab {
	appl_t *		appl_root;
	unsigned		appl_count;
	spinlock_t		lock;
	struct list_head	ncci_head;
} appltab_t;

void table_init(appltab_t *tab);
void table_exit(appltab_t *tab);

bool appl_alive(appltab_t *tab, unsigned appl);

capiinfo_t handle_data_conf(appltab_t *tab, queue_t *q, unsigned char *m);

void *data_by_id(unsigned appl_id);
unsigned attr_by_id(unsigned appl_id);
void *first_data(int *res);
void *next_data(int *res);
int appl_profile(unsigned appl_id, unsigned *bs, unsigned *bc);

void new_ncci(unsigned appl_id, __u32 ncci, unsigned winsize, unsigned blksize);
void free_ncci(unsigned appl_id, __u32 ncci);

unsigned char *data_block(unsigned appl_id, __u32 ncci, unsigned handle);

void register_appl(struct capi_ctr *ctr, u16 appl, capi_register_params *args);
void release_appl(struct capi_ctr *ctr, u16 appl);
u16 send_msg(struct capi_ctr *ctr, struct sk_buff *skb);

#endif
