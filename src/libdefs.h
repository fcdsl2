/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2002, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#ifndef __have_libdefs_h__
#define __have_libdefs_h__

#include <linux/time.h>
#include <stdarg.h>
#include "defs.h"

#if defined (DRIVER_TYPE_DSL_RAP)
#include "common.h"
#endif

typedef enum {
	OSTIMER_END	= 0,
	OSTIMER_RESTART	= 1,
	OSTIMER_RENEW	= 2
} ostimer_return_t;

typedef ostimer_return_t (asmlinkage * timerfunc_t) (unsigned long);

typedef struct __data {
	unsigned num;
	char *buffer;
} appldata_t;

typedef struct __lib {
	void (asmlinkage * init) (unsigned,
				  void (asmlinkage *) (void *, unsigned),
				  void (asmlinkage *) (void *),
				  void (asmlinkage *) (void));
	
	char * (asmlinkage * params) (void);
	
	int (asmlinkage * get_message) (unsigned char *);
	void (asmlinkage * put_message) (unsigned char *);
	
	unsigned char * (asmlinkage * get_data_block) (unsigned, 
						unsigned long, unsigned);
	void (asmlinkage * free_data_block) (unsigned, unsigned char *);
	
	int (asmlinkage * new_ncci) (unsigned, unsigned long, unsigned, unsigned);
	void (asmlinkage * free_ncci) (unsigned, unsigned long);
	
	unsigned (asmlinkage * block_size) (unsigned);
	unsigned (asmlinkage * window_size) (unsigned);
	unsigned (asmlinkage * card) (void);
	
	void * (asmlinkage * appl_data) (unsigned);
#if defined (DRIVER_TYPE_DSL_RAP)
	unsigned (asmlinkage * appl_attr) (unsigned);
#endif
	appldata_t * (asmlinkage * appl_1st_data) (appldata_t *);
	appldata_t * (asmlinkage * appl_next_data) (appldata_t *);
	
	void * (asmlinkage * malloc) (unsigned);
	void (asmlinkage * free) (void *);
#if defined (DRIVER_TYPE_DSL) && !defined (DRIVER_TYPE_DSL_USB)
	void * (asmlinkage * malloc2) (unsigned);
#endif

#if defined (DRIVER_TYPE_DSL_RAP)
	void (asmlinkage * delay) (unsigned);
#endif
	unsigned long (asmlinkage * msec) (void);
	unsigned long long (asmlinkage * msec64) (void);
	
	int (asmlinkage * timer_new) (unsigned);
	void (asmlinkage * timer_delete) (void);
	int (asmlinkage * timer_start) (unsigned, unsigned long, 
					unsigned long, timerfunc_t);
	int (asmlinkage * timer_stop) (unsigned);
	void (asmlinkage * timer_poll) (void);
#if defined (DRIVER_TYPE_DSL)
	int (asmlinkage * get_time) (struct timeval *);
#endif

#if defined (DRIVER_TYPE_DSL_TM) || defined (DRIVER_TYPE_DSL_USB)
	void (asmlinkage * dprintf) (char *, ...);
#endif
#if defined (DRIVER_TYPE_DSL_RAP)
	void (asmlinkage * printf) (char *, va_list);
	void (asmlinkage * putf) (char *, va_list);
#else
	void (asmlinkage * printf) (char *, va_list);
#endif
	void (asmlinkage * puts) (char *);
	void (asmlinkage * putl) (long);
	void (asmlinkage * puti) (int);
	void (asmlinkage * putc) (char);
	void (asmlinkage * putnl) (void);
	
	void (asmlinkage * _enter_critical) (const char *, int);
	void (asmlinkage * _leave_critical) (const char *, int);
	void (asmlinkage * enter_critical) (void);
	void (asmlinkage * leave_critical) (void);
	void (asmlinkage * enter_cache_sensitive_code) (void);
	void (asmlinkage * leave_cache_sensitive_code) (void);

#if defined (DRIVER_TYPE_DSL_RAP)
	void (asmlinkage * xfer_req) (dif_require_p, unsigned);
#endif

	char *		name;
	unsigned	udata;
	void *		pdata;
} lib_interface_t, *lib_interface_p;

typedef struct __f {
	unsigned	nfuncs;
	void	     (asmlinkage * sched_ctrl) (unsigned);
	void         (asmlinkage * wakeup_ctrl) (unsigned);
	void         (asmlinkage * version_ind) (char *);
	unsigned     (asmlinkage * sched_suspend) (unsigned long);
	unsigned     (asmlinkage * sched_resume) (void);
	unsigned     (asmlinkage * ctrl_remove) (void);
	unsigned     (asmlinkage * ctrl_add) (void);
} functions_t, * functions_p;

typedef struct __cb {
	unsigned (asmlinkage * cm_start) (void);
	char * (asmlinkage * cm_init) (unsigned, unsigned);
	int (asmlinkage * cm_activate) (void);
	int (asmlinkage * cm_exit) (void);
	unsigned (asmlinkage * cm_handle_events) (void);
	int (asmlinkage * cm_schedule) (void);
	void (asmlinkage * cm_timer_irq_control) (unsigned);
	void (asmlinkage * cm_register_ca_functions) (functions_p);
#if !defined (DRIVER_TYPE_DSL_RAP)
	unsigned (asmlinkage * check_controller) (unsigned, unsigned *);
#endif

#if defined (DRIVER_TYPE_DSL_TM)
	int (asmlinkage * cc_debugging_needed) (void);
	unsigned (asmlinkage * cc_num_link_buffer) (int);
	unsigned (asmlinkage * cc_link_version) (void);
	void (asmlinkage * cc_compress_code) (void);
	int (asmlinkage * cc_status) (unsigned, void *, void *);
	int (asmlinkage * cc_run) (void);
#endif
	
#if defined (DRIVER_TYPE_DSL_RAP)
	int (asmlinkage * cc_debugging_needed) (void);
#if !defined (DRIVER_TYPE_USB)
	void (asmlinkage * cc_init_debug) (void *, unsigned);
	void (asmlinkage * cc_init_dma) (dma_list_t, unsigned, 
						dma_list_t, unsigned);
#endif
	unsigned (asmlinkage * cc_num_link_buffer) (int);
	unsigned (asmlinkage * cc_link_version) (void);
#if !defined (DRIVER_TYPE_USB)
	unsigned (asmlinkage * cc_timer_offset) (void);
	unsigned (asmlinkage * cc_pc_ack_offset) (void);
	unsigned (asmlinkage * cc_buffer_params) (unsigned *, ioaddr_p *, 
						unsigned *, unsigned *);
#endif
	void (asmlinkage * cc_compress_code) (void);
	int (asmlinkage * cc_status) (unsigned, void *, void *);
	int (asmlinkage * cc_run) (void);
#endif

} lib_callback_t, *lib_callback_p;

#endif
