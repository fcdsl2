/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2002, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#ifndef __have_defs_h__
#define __have_defs_h__

#include <linux/version.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,31)
#define capi_ctr_down capi_ctr_reseted
#endif

#if defined (__fcdslsl__)
# define PCI_DEVICE_ID_FCDSL2	0x2700
# define PRODUCT_LOGO		"AVM FRITZ!Card DSL SL"
#elif defined (__fcdsl2__)
# define PCI_DEVICE_ID_FCDSL2	0x2900
# define PRODUCT_LOGO		"AVM FRITZ!Card DSL v2.0"
# define DRIVER_TYPE_ISDN
#else
# error Card specifier missing!
#endif
#define INTERFACE		"pci"

#define SHORT_LOGO		KBUILD_MODNAME "-" INTERFACE
#define DRIVER_LOGO		PRODUCT_LOGO " driver"
#define	DRIVER_TYPE_DSL
#define	DRIVER_TYPE_DSL_RAP

#define	TOOLS_MEM_DUMP
#define	TOOLS_PTR_QUEUE

#endif
