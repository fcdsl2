/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2002, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#include <linux/slab.h>
#include <linux/string.h>
#include <linux/vmalloc.h>
#include <linux/kernel.h>
#include "defs.h"
#include "libdefs.h"
#include "driver.h"
#include "lib.h"
#include "tools.h"

#if defined (TOOLS_PTR_QUEUE)
struct __q {
	unsigned	head, tail, mask;
	unsigned	size, free;
	unsigned	bnum, blen;
	void **		item;
	char *		bptr;
};
#endif

#if defined (TOOLS_PTR_QUEUE)
ptr_queue_p q_make(unsigned max)
{
	unsigned mask = 1;
	ptr_queue_p qp;

	qp = kmalloc(sizeof(ptr_queue_t), GFP_KERNEL);
	if (!qp)
		return NULL;

	qp->item = kmalloc(max * sizeof(void *), GFP_KERNEL);
	if (!qp->item) {
		kfree(qp);
		return NULL;
	}
	qp->bptr = NULL;
	while (mask < max) {
		mask <<= 1;
	}
	assert (mask == max);
	--mask;
	qp->head = 0;
	qp->tail = 0;
	qp->mask = mask;
	qp->size = max;
	qp->free = max;
	return qp;
}

void q_remove(ptr_queue_p *qpp)
{
	ptr_queue_p qp;
	
	assert (qpp != NULL);
	qp = *qpp;
	assert (qp != NULL);
	assert (qp->item != NULL);

	kfree(qp->bptr);
	kfree(qp->item);
	kfree(qp);
	*qpp = NULL;
}

void q_reset(ptr_queue_p qp)
{
	assert (qp != NULL);
	qp->head = qp->tail = 0;
	qp->free = qp->size;
}

bool q_attach_mem(ptr_queue_p qp, unsigned n, unsigned len)
{
	void *buf;

	assert (qp != NULL);
	assert (qp->bptr == 0);
	assert ((n * len) != 0);

	buf = kmalloc(n * len, GFP_KERNEL);
	if (!buf)
		return false;
	qp->bnum = n;
	qp->blen = len;
	qp->bptr = buf;
	return true;
}

bool q_enqueue(ptr_queue_p qp, void *p)
{
	assert (qp != NULL);
	if (qp->free == 0) {
		return false;
	}
	assert (qp->head < qp->size);
	qp->item[qp->head++] = p;
	qp->head &= qp->mask;
	assert (qp->head < qp->size);
	qp->free--;
	return true;
}

bool q_enqueue_mem(ptr_queue_p qp, void *m, unsigned len)
{
	unsigned	ix;
	char *		mp;
	
	assert (qp != NULL);
	assert (qp->bptr != NULL);
	assert (qp->blen >= len);
	if (qp->free == 0) {
		return false;
	}
	assert (qp->head < qp->size);
	ix = qp->head++;
	qp->head &= qp->mask;
	qp->item[ix] = mp = &qp->bptr[ix * len];
	assert (mp != NULL);
	memcpy(mp, m, len);
	assert (qp->head < qp->size);
	qp->free--;
	return true;
}

bool q_dequeue(ptr_queue_p qp, void **pp)
{
	assert (qp != NULL);
	if (qp->free == qp->size) {
		return false;
	}
	assert (qp->tail < qp->size);
	assert (pp != NULL);
	*pp = qp->item[qp->tail++];
	qp->tail &= qp->mask;
	assert (qp->tail < qp->size);
	qp->free++;
	return true;
}

bool q_peek(ptr_queue_p qp, void **pp)
{
	assert (qp != NULL);
	if (qp->free == qp->size) {
		return false;
	}
	assert (qp->tail < qp->size);
	assert (pp != NULL);
	*pp = qp->item[qp->tail];
	return true;
}

unsigned q_get_count(ptr_queue_p qp)
{
	assert (qp != NULL);
	return qp->size - qp->free;
}
#endif

#if defined (TOOLS_MEM_DUMP)
void memdump(const void *mem, unsigned len, unsigned start, const char *msg)
{
        unsigned        max, min, idx;
        unsigned char * data = (unsigned char *) mem;
        char            hex[50], chr[20];

	fritz_dbg("Memory dump %s:\n", msg);
        min = 0;
        while (min < len) {
                max = ((min + 16) > len ? len : min + 16);
                idx = 0;
                while ((min + idx) < max) {
                        snprintf (hex + 3 * idx, 4, "%02x ", *data);
                        snprintf (chr + idx, 2, "%c", ((' ' <= *data) &&
                                        (*data <= '~')) ? *data : '.');
                        ++idx;
                        ++data;
                }
                while (idx < 16)
                        strcpy(hex + 3 * idx++, "   ");
                fritz_dbg("%08x: %s  %s\n", min + start, hex, chr);
                min = max;
        }
}
#endif
