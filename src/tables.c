/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2002, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <linux/list.h>
#include <linux/capi.h>
#include <linux/kernelcapi.h>
#include <linux/isdn/capicmd.h>
#include <linux/isdn/capiutil.h>
#include <linux/isdn/capilli.h>
#include "defs.h"
#include "lib.h"
#include "driver.h"
#include "tools.h"
#include "queue.h"
#include "tables.h"

static void create_ncci(appltab_t *tab, appl_t *appp, ncci_t *nccip,
			NCCI_t ncci, unsigned wsize, unsigned bsize,
			unsigned char **data)
{
	fritz_dbg("New NCCI(%x), window size %u...\n", ncci, wsize);

	capilib_new_ncci(&tab->ncci_head, appp->id, ncci, wsize);

	nccip->ncci = ncci;
	nccip->appl = appp->id;
	nccip->win_size = wsize;
	nccip->blk_size = bsize;
	nccip->data = data;
	nccip->pred = NULL;

	nccip->succ = appp->root;
	appp->root = nccip;
	if (nccip->succ)
		nccip->succ->pred = nccip;

	appp->nncci++;
}

static void remove_ncci(appltab_t *tab, appl_t *appp, ncci_t *nccip)
{
	unsigned i;

	assert (appp);
	assert (nccip);

	fritz_dbg("Remove NCCI(%x), appl %u...\n", nccip->ncci, appp->id);

	capilib_free_ncci(&tab->ncci_head, appp->id, nccip->ncci);

	if (nccip->succ)
		nccip->succ->pred = nccip->pred;
	if (nccip->pred)
		nccip->pred->succ = nccip->succ;
	else
		appp->root = nccip->succ;
	appp->nncci--;

	for (i = 0; i < appp->blk_count; i++)
		kfree(nccip->data[i]);
	kfree(nccip->data);
	kfree(nccip);
}

static appl_t *search_appl(appltab_t *tab, unsigned id)
{
	appl_t *appp;

	appp = tab->appl_root;
	while (appp != NULL) {
		if (appp->id == id) {
			break;
		}
		appp = appp->succ;
	}
	info (appp);
	return appp;
}

static int create_appl(appltab_t *tab, unsigned id, unsigned ncount,
		       unsigned bcount, unsigned bsize, void *data)
{
	appl_t *appp;
	int err;

	appp = kzalloc(sizeof(appl_t), GFP_KERNEL);
	if (!appp) {
		fcdsl2_err("Not enough memory for application record.\n");
		return -ENOMEM;
	}
	appp->id         = id;
	appp->ncci_count = ncount;
	appp->blk_count  = bcount;
	appp->blk_size   = bsize;
	appp->data       = data;

	spin_lock_bh(&tab->lock);

	if (!search_appl(tab, id)) {
		appp->succ = tab->appl_root;
		tab->appl_root = appp;
		if (appp->succ)
			appp->succ->pred = appp;
		tab->appl_count++;
		err = 0;
	} else {
		err = -EEXIST;
	}

	spin_unlock_bh(&tab->lock);

	return err;
}

static void remove_appl(appltab_t *tab, appl_t *appp)
{
	ncci_t *nccip;
	ncci_t *tmp;

	assert (appp);

	if (appp->pred)
		appp->pred->succ = appp->succ;
	else
		tab->appl_root = appp->succ;
	if (appp->succ)
		appp->succ->pred = appp->pred;
	nccip = appp->root;
	tab->appl_count--;

	while (nccip != NULL) {
		tmp = nccip->succ;
		remove_ncci(tab, appp, nccip);
		nccip = tmp;
	}
	capilib_release_appl(&tab->ncci_head, appp->id);

	kfree(appp->data);
	kfree(appp);
}

static ncci_t *locate_ncci(appltab_t *tab, appl_t *appp, NCCI_t ncci)
{
	ncci_t *tmp;

	assert (tab != NULL);
	assert (appp != NULL);

	tmp = appp->root;
	while (tmp && tmp->ncci != ncci)
		tmp = tmp->succ;
	info (tmp);
	return tmp;
}

static appl_t *get_appl(appltab_t *tab, unsigned ix)
{
	appl_t *appp = NULL;

	assert (ix < tab->appl_count);

	if (ix < tab->appl_count) {
		appp = tab->appl_root;
		while (ix > 0) {
			appp = appp->succ;
			--ix;
		}
	}

	return appp;
}

bool appl_alive(appltab_t *tab, unsigned appl)
{
	appl_t *appp;
	bool alive = false;

	spin_lock_bh(&tab->lock);

	appp = search_appl(tab, appl);
	if (appp)
		alive = !appp->dying;

	spin_unlock_bh(&tab->lock);

	return alive;
}

void table_init(appltab_t *tab)
{
	tab->appl_root  = NULL;
	tab->appl_count = 0;
	spin_lock_init(&tab->lock);
	INIT_LIST_HEAD(&tab->ncci_head);
}

void table_exit(appltab_t *tab)
{
	appl_t *appp;
	appl_t *tmp;

	spin_lock_bh(&tab->lock);

	appp = tab->appl_root;
	while (appp) {
		tmp = appp->succ;
		remove_appl(tab, appp);
		appp = tmp;
	}

	spin_unlock_bh(&tab->lock);
}

static capiinfo_t handle_message(appltab_t *tab, queue_t *q,
				 struct sk_buff *msg)
{
	capiinfo_t ci = CAPI_NOERROR;
	bool charge = false;
	unsigned appl;
	appl_t *appp;

	assert (tab != NULL);
	assert (q != NULL);
	assert (msg != NULL);

	appl = CAPIMSG_APPID(msg->data);

	spin_lock_bh(&tab->lock);

	appp = search_appl(tab, appl);
	if (!appp) {
		fcdsl2_err("Unknown application id! (%u)\n", appl);
		ci = CAPI_ILLAPPNR;
		goto unlock_out;
	}
	switch (CAPIMSG_CMD(msg->data)) {
	case CAPI_DATA_B3_REQ:
		ci = capilib_data_b3_req(&tab->ncci_head, appl,
					 CAPIMSG_NCCI(msg->data),
					 CAPIMSG_MSGID(msg->data));
		charge = true;
		break;

	case 0xFE80:
		appp->dying = true;
	default:
		break;
	}

unlock_out:
	spin_unlock_bh(&tab->lock);

	if (ci == CAPI_NOERROR && queue_put(q, msg, charge) < 0)
		ci = CAPI_SENDQUEUEFULL;

	return ci;
}

capiinfo_t handle_data_conf(appltab_t *tab, queue_t *q, unsigned char *m)
{
	unsigned appl, hand;
	NCCI_t ncci;
	
	assert (tab != NULL);
	assert (q != NULL);
	assert (m != NULL);
	assert (CAPIMSG_CMD(m) == CAPI_DATA_B3_CONF);

	appl = CAPIMSG_APPID(m);
	ncci = CAPIMSG_NCCI(m);
	hand = CAPIMSG_U16(m, 12);

	spin_lock_bh(&tab->lock);
	capilib_data_b3_conf(&tab->ncci_head, appl, ncci, CAPIMSG_MSGID(m));
	spin_unlock_bh(&tab->lock);

	queue_unpark_free(q, appl, ncci, hand);

	return CAPI_NOERROR;
}

static unsigned char *
ncci_data_buffer(appltab_t *tab, appl_t *appp, NCCI_t ncci, unsigned index,
		 void *data)
{
	ncci_t *nccip;

	assert (tab != NULL);
	assert (appp != NULL);

	nccip = locate_ncci(tab, appp, ncci);
	if (!nccip) {
		fritz_dbg("Data buffer request failed. NCCI not found.\n");
		return NULL;
	}
	if (index >= appp->blk_count) {
		fritz_dbg("Data buffer index out of range.\n");
		return NULL;
	}

	if (!nccip->data[index])
		nccip->data[index] = data;

	return nccip->data[index];
}

static struct sk_buff *make_0xfe_request(unsigned appl)
{
	struct sk_buff *skb;
	unsigned char *req;

	skb = alloc_skb(8, GFP_KERNEL);
	if (!skb) {
		fcdsl2_err("Unable to allocate message buffer.\n");
		return NULL;
	}

	req = skb_put(skb, 8);
	req[0] = 8;
	req[1] = 0;
	req[2] = appl & 0xFF;
	req[3] = (appl >> 8) & 0xFF;
	req[4] = 0xFE;
	req[5] = 0x80;

	return skb;
}

void *data_by_id(unsigned appl_id)
{
	void *data = NULL;
	appl_t *appp;

	assert (capi_card != NULL);

	spin_lock_bh(&capi_card->appls.lock);

	appp = search_appl(&capi_card->appls, appl_id);
	if (appp)
		data = appp->data;

	spin_unlock_bh(&capi_card->appls.lock);

	/*
	 * The pointer returned here is supposed to remain valid until the
	 * stack confirmed an application removal and we ran remove_appl.
	 * Removal must happen under stack_lock, and so this service requires
	 * the same protection.
	 */
	WARN_ON_ONCE(!mutex_is_locked(&stack_lock));

	return data;
}

#if defined (DRIVER_TYPE_DSL_RAP)
unsigned attr_by_id(unsigned appl_id)
{
	unsigned attr = 0;
	appl_t * appp;

	spin_lock_bh(&capi_card->appls.lock);

	appp = search_appl(&capi_card->appls, appl_id);	
	if (appp)
		attr = appp->attr;

	spin_unlock_bh(&capi_card->appls.lock);

	return attr;
}
#endif

void *first_data(int *res)
{
	void *data = NULL;
	appl_t *appp;

	assert (capi_card != NULL);
	assert (res != NULL);

	*res = -1;

	spin_lock_bh(&capi_card->appls.lock);

	appp = capi_card->appls.appl_root;
	if (appp) {
		*res = 0;
		data = appp->data;
	}

	spin_unlock_bh(&capi_card->appls.lock);

	return data;
}

void *next_data(int *res)
{
	void *data = NULL;
	appl_t * appp;

	assert (capi_card != NULL);
	assert (res != NULL);

	spin_lock_bh(&capi_card->appls.lock);

	appp = get_appl(&capi_card->appls, *res);
	if (appp)
		appp = appp->succ;

	if (appp) {
		data = appp->data;
		(*res)++;
	} else
		*res = -1;

	spin_unlock_bh(&capi_card->appls.lock);

	return data;
}

int appl_profile(unsigned appl_id, unsigned *bs, unsigned *bc)
{
	appl_t *appp;
	int res = 0;

	assert (capi_card != NULL);

	spin_lock_bh(&capi_card->appls.lock);

	appp = search_appl(&capi_card->appls, appl_id);
	if (appp) {
		res = 1;
		if (bs)
			*bs = appp->blk_size;
		if (bc)
			*bc = appp->blk_count;
	}

	spin_unlock_bh(&capi_card->appls.lock);

	return res;
}

void new_ncci(unsigned appl_id, __u32 ncci, unsigned winsize, unsigned blksize)
{
	unsigned char **data;
	unsigned blk_count;
	ncci_t *nccip;
	appl_t *appp;

	assert (capi_card != NULL);

	MLOG("NEW NCCI(appl:%u,ncci:%X)\n", appl_id, ncci);

	if (!appl_profile(appl_id, NULL, &blk_count)) {
		fcdsl2_err("Unknown application id #%u\n", appl_id);
		return;
	}
	nccip = kmalloc(sizeof(ncci_t), GFP_KERNEL);
	if (!nccip) {
		fcdsl2_err("Failed to allocate NCCI record.\n");
		return;
	}
	data = kzalloc(sizeof(unsigned char *) * blk_count, GFP_KERNEL);
	if (!data) {
		fcdsl2_err("Failed to allocate data buffer directory.\n");
		kfree(nccip);
		return;
	}

	spin_lock_bh(&capi_card->appls.lock);

	appp = search_appl(&capi_card->appls, appl_id);
	if (!appp) {
		fcdsl2_err("Application id #%u became invalid\n", appl_id);
		goto unlock_out;
	}
	if (WARN_ON(blk_count != appp->blk_count)) {
		kfree(nccip);
		kfree(data);
		goto unlock_out;
	}
	create_ncci(&capi_card->appls, appp, nccip, (NCCI_t)ncci, winsize,
		    blksize, data);

unlock_out:
	spin_unlock_bh(&capi_card->appls.lock);
}

void free_ncci(unsigned appl_id, __u32 ncci)
{
	appl_t *appp;
	ncci_t *nccip;

	assert (capi_card != NULL);

	spin_lock_bh(&capi_card->appls.lock);

	appp = search_appl(&capi_card->appls, appl_id);
	if (!appp) {
		fcdsl2_err("Unknown application id #%u\n", appl_id);
		goto unlock_out;
	}
	if (0xFFFFFFFF == ncci) {		/* 2nd phase RELEASE */
		MLOG("FREE APPL(appl:%u)\n", appl_id);
		remove_appl(&capi_card->appls, appp);
	} else {
		nccip = locate_ncci(&capi_card->appls, appp, ncci);
		if (nccip) {
			MLOG("FREE NCCI(appl:%u,ncci:%X)\n", appl_id, ncci);
			remove_ncci(&capi_card->appls, appp, nccip);
		} else {
			fcdsl2_err("Attempt to free unknown NCCI.\n");
		}
	}

unlock_out:
	spin_unlock_bh(&capi_card->appls.lock);
}

unsigned char *data_block(unsigned appl_id, __u32 ncci, unsigned handle)
{
	bool allocate_new = false;
	void *new_data = NULL;
	unsigned blk_size = 0;
	void *data;
	appl_t *appp;

	assert (capi_card != NULL);

retry:
	if (allocate_new) {
		new_data = kmalloc(blk_size, GFP_KERNEL);
		if (!new_data) {
			fcdsl2_err("Not enough memory for data buffer.\n");
			return NULL;
		}
	}

	spin_lock_bh(&capi_card->appls.lock);

	appp = search_appl(&capi_card->appls, appl_id);
	if (!appp) {
		spin_unlock_bh(&capi_card->appls.lock);
		fcdsl2_err("Unknown application id #%u\n", appl_id);
		return NULL;
	}
	if (!WARN_ON(allocate_new && blk_size != appp->blk_size)) {
		blk_size = appp->blk_size;
		data = ncci_data_buffer(&capi_card->appls, appp, ncci, handle,
					new_data);
	}

	spin_unlock_bh(&capi_card->appls.lock);

	if (!data && !allocate_new) {
		allocate_new = true;
		goto retry;
	}
	if (allocate_new && data != new_data)
		/* We must have lost the race with another caller. */
		kfree(new_data);

	return data;
}

u16 send_msg(struct capi_ctr *ctr, struct sk_buff *skb)
{
	struct fritz_card *card;
	capiinfo_t ci;

	card = capi2fritz_card(ctr);
	ci = handle_message(&card->appls, &card->queue, skb);
	kick_stack_thread(card, false, true);
	return ci;
}

void register_appl(struct capi_ctr *ctr, u16 appl, capi_register_params *args)
{
	struct fritz_card *card;
	void *ptr;
	unsigned nc;
	int err;

	MLOG("REGISTER(appl:%u)\n", appl);
	card = capi2fritz_card(ctr);

	if ((int) args->level3cnt < 0) {
		nc = nbchans(ctr) * -((int) args->level3cnt);
	} else {
		nc = args->level3cnt;
	}
	if (0 == nc) {
		nc = nbchans(ctr);
	}

	ptr = kzalloc(card->length, GFP_KERNEL);
	if (!ptr) {
		fcdsl2_err("Not enough memory for application data.\n");
		return;
	}

	err = create_appl(&card->appls, appl, nc, args->datablkcnt,
			  args->datablklen, ptr);
	if (err) {
		if (err != -EEXIST)
			fritz_dbg("Unable to create application record.\n");
		kfree(ptr);
		return;
	}

	/*
	 * Hold the stack_lock across application creation/destruction,
	 * specifically while calling into the stack via those hooks.
	 */
	mutex_lock(&stack_lock);
	(*card->reg_func)(ptr, appl);
	mutex_unlock(&stack_lock);
}

void release_appl(struct capi_ctr *ctr, u16 appl)
{
	struct fritz_card *card = capi2fritz_card(ctr);
	struct sk_buff *skb;
	appl_t *appp;
	void *data;
	capiinfo_t res;
	
	MLOG("RELEASE(appl:%u)\n", appl);

	/* See register_appl. */
	mutex_lock(&stack_lock);

	spin_lock_bh(&card->appls.lock);

	appp = search_appl(&card->appls, appl);
	if (!appp || appp->dying) {
		spin_unlock_bh(&card->appls.lock);
		mutex_unlock(&stack_lock);
		return;
	}
	data = appp->data;
	spin_unlock_bh(&card->appls.lock);

	if (card->rel_func)
		(*capi_card->rel_func)(appp->data);

	mutex_unlock(&stack_lock);

	skb = make_0xfe_request(appl);
	if (skb) {
		res = handle_message(&card->appls, &card->queue, skb);
		info (res == CAPI_NOERROR);
		kick_stack_thread(card, false, true);
	}
}
