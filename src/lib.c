/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2002, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#include <linux/mm.h>
#include <linux/kernel.h>
#include <linux/wait.h>
#include <linux/jiffies.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <stdarg.h>
#include "defs.h"
#include "driver.h"
#include "tables.h"
#include "queue.h"
#include "tools.h"
#include "libstub.h"
#include "lib.h"

#if defined (DRIVER_TYPE_DSL_RAP)
#include "devif.h"
#endif

#define PRINTF_BUFFER_SIZE	1024

#define JIFF2MSEC		(1000/HZ)

short int vcc = 0x01;
short int vpi = 0x01;
short int vci = 0x20;

module_param(vcc, short, 0);
module_param(vpi, short, 0);
module_param(vci, short, 0);

MODULE_PARM_DESC(vcc, "Virtual Channel Connection");
MODULE_PARM_DESC(vpi, "Virtual Path Identifier");
MODULE_PARM_DESC(vci, "Virtual Channel Identifier");

#if defined (LOG_STACK_MSG)
#include <linux/isdn/capicmd.h>
#include <linux/isdn/capiutil.h>

#define	VEC_BITLEN		(sizeof(unsigned)*8)
#define	VEC_LENGTH		(256/VEC_BITLEN)
#define	VEC_MSGIDX(c)		(((c)&255)/VEC_BITLEN)
#define	VEC_MSGMSK(c)		(1<<(((c)&255)%VEC_BITLEN))

static unsigned			log_set[VEC_LENGTH] = { 0, } ;

#define	LOG_TRACE_MSG(c)	log_set[VEC_MSGIDX(c)]|=VEC_MSGMSK(c)
#define	LOG_MSG_TRACED(m)	(0!=(log_set[VEC_MSGIDX(CAPIMSG_COMMAND(m))] \
					&VEC_MSGMSK(CAPIMSG_COMMAND(m))))

static void dump_message (unsigned char * m, const char * logo) {
	unsigned short	len;
	
	len = CAPIMSG_LEN(m);
	fritz_dbg("CAPIMSG - L:%u A:%04x C:%02x S:%02x N:%04x\n",
		  len, CAPIMSG_APPID(m), CAPIMSG_COMMAND(m),
		  CAPIMSG_SUBCOMMAND(m), CAPIMSG_MSGID(m)
	);
#if defined (LOG_STACK_MSG_DUMP)
#if !defined (TOOLS_MEM_DUMP)
#error Function memdump() required!
#endif
	if (len > 8) {
		memdump (m + 8, len - 8, 8, logo);
	}
#endif
} /* dump_message */
#endif

/* All NOPs. The stack is fully serialized by the stack_lock. */
static asmlinkage void os__enter_critical(const char *f, int l) { }
static asmlinkage void os__leave_critical(const char * f, int l) { }
static asmlinkage void os_enter_critical(void) { }
static asmlinkage void os_leave_critical(void) { }

static asmlinkage void os_enter_cache_sensitive_code(void) { /* NOP */ }
static asmlinkage void os_leave_cache_sensitive_code(void) { /* NOP */ }

static asmlinkage void 
os_init(unsigned len, void (asmlinkage * reg) (void *, unsigned),
	void (asmlinkage * rel) (void *), void (asmlinkage * dwn) (void))
{
	init (len, reg, rel, dwn);
}

static asmlinkage char *os_params(void)
{
#if defined (DRIVER_TYPE_DSL)
	static char parmbuf[128] =
		":ATMVCC\0\\x0000\0:ATMVPI\0\\x0000\0:ATMVCI\0\\x0000\0\0";
	static bool parmbuf_ready;

	fritz_dbg("Using VCC/VPI/VCI = 0x%x/0x%x/0x%x\n", vcc, vpi, vci);
	if (!parmbuf_ready) {
		sprintf(parmbuf+11, "%04x", vcc);
		sprintf(parmbuf+26, "%04x", vpi);
		sprintf(parmbuf+41, "%04x", vci);
		parmbuf_ready = true;
	}
	return parmbuf;
#else
	return NULL;
#endif
}

static asmlinkage int os_get_message(unsigned char *msg)
{
	int res;

	assert (msg != NULL);
	res = msg2stack (msg); 
#if defined (LOG_STACK_MSG)
	if ((res != 0) && LOG_MSG_TRACED(msg)) {
		dump_message (msg, "GET_MESSAGE");
	}
#endif
	return res;
}

static asmlinkage void os_put_message(unsigned char *msg)
{
	assert (msg != NULL);
#if defined (LOG_STACK_MSG)
	if (LOG_MSG_TRACED(msg)) {
		dump_message (msg, "PUT_MESSAGE");
	}
#endif
	msg2capi (msg);
}

static asmlinkage unsigned char *
os_get_data_block(unsigned appl, unsigned long ncci, unsigned index)
{
	char *res;

	res = data_block (appl, ncci, index);
	return res;
}

static asmlinkage void os_free_data_block(unsigned appl, unsigned char *data)
{
}

static asmlinkage int os_new_ncci(unsigned appl, unsigned long ncci,
				  unsigned win_size, unsigned blk_size)
{
	new_ncci (appl, ncci, win_size, blk_size); 
	return 1;
}

static asmlinkage void os_free_ncci(unsigned appl, unsigned long ncci)
{
	free_ncci (appl, ncci);
}

static asmlinkage unsigned os_block_size(unsigned appl)
{
	unsigned bs, dummy;

	appl_profile (appl, &bs, &dummy);
	return bs;
}

static asmlinkage unsigned os_window_size(unsigned appl)
{
	unsigned bc, dummy;

	appl_profile (appl, &dummy, &bc);
	return bc;
}

static asmlinkage unsigned os_card(void)
{
	return 0;
}

static asmlinkage void *os_appl_data(unsigned appl)
{
	void * res;

	res = data_by_id (appl);
	return res;
}

#if defined (DRIVER_TYPE_DSL_RAP)
static asmlinkage unsigned os_appl_attr (unsigned appl)
{
	unsigned res;

	res = attr_by_id (appl);
	return res;
}
#endif

static asmlinkage appldata_t *os_appl_1st_data(appldata_t *s)
{
	int		num;
	void *		data;
	static char	e[10]; 

	memset(&e, 0, 10);
	data = first_data (&num);
	s->num    = num;
	s->buffer = (NULL == data) ? e : data;
	return s;
}

static asmlinkage appldata_t *os_appl_next_data(appldata_t *s)
{
	int		num;
	void *		data;
	static char	e[10]; 

	memset(&e, 0, 10);
	if ((num = s->num) < 0) {	
		return NULL;
	}
	data = next_data (&num);
	s->num    = num;
	s->buffer = (NULL == data) ? e : data;
	return s;
}

static asmlinkage void *os_malloc(unsigned len)
{
	void *mem;

	if (len <= PAGE_SIZE)
		return kzalloc(len, GFP_KERNEL);

	mem = vmalloc(len);
	if (mem)
		memset(mem, 0, len);

	return mem;
}

static asmlinkage void os_free(void *p)
{
	if (is_vmalloc_addr(p))
		vfree(p);
	else
		kfree(p);
}

#if defined (DRIVER_TYPE_DSL_RAP) || defined (DRIVER_TYPE_DSL_USB)
static asmlinkage void os_delay(unsigned msec)
{
	msleep(msec);
}
#endif

static asmlinkage unsigned long long os_msec64(void)
{
	return get_jiffies_64() * JIFF2MSEC;
}

static asmlinkage unsigned long os_msec(void)
{
	return get_jiffies_64() * JIFF2MSEC;
}

typedef struct {
	struct timer_list	kernel_timer;
	timerfunc_t		func;
	unsigned long		arg;
	unsigned long		timeout_jiffies;
} timer_rec_t;

static timer_rec_t *timer;
static unsigned timer_count;

static void timer_handler(unsigned long unused)
{
	kick_stack_thread(capi_card, false, true);
}

static asmlinkage int os_timer_new(unsigned ntimers)
{
	unsigned size;
	unsigned i;

	assert (ntimers > 0); 

	timer_count = ntimers;
	size = sizeof(timer_rec_t) * ntimers;
	timer = kzalloc(size, GFP_KERNEL);
	info (timer != NULL);
	if (!timer) {
		timer_count = 0;
		return 1;
	}
	for (i = 0; i < ntimers; i++)
		setup_timer(&timer[i].kernel_timer, timer_handler, 0);
	return 0;
}

static asmlinkage void os_timer_delete(void)
{
	unsigned i;

	for (i = 0; i < timer_count; i++)
		del_timer_sync(&timer[i].kernel_timer);

	kfree(timer);
	timer = NULL;
	timer_count = 0;
}

static asmlinkage int os_timer_start(unsigned index, unsigned long timeout,
				     unsigned long arg, timerfunc_t func)
{
	assert (index < timer_count);
	if (index >= timer_count)
		return 1;

	timer[index].func = func;
	timer[index].arg = arg;
	timer[index].timeout_jiffies = msecs_to_jiffies(timeout);
	mod_timer(&timer[index].kernel_timer,
		  jiffies + timer[index].timeout_jiffies);
	return 0;
}

static asmlinkage int os_timer_stop(unsigned index)
{
	assert (index < timer_count);
	if (index >= timer_count)
		return 1;

	del_timer(&timer[index].kernel_timer);
	timer[index].func = NULL;
	return 0;
}

void asmlinkage os_timer_poll(void)
{
	ostimer_return_t ret;
	unsigned i;

	if (!timer)
		return;

	for (i = 0; i < timer_count; i++)
		if (timer[i].func && !timer_pending(&timer[i].kernel_timer)) {
			ret = (*timer[i].func)(timer[i].arg);
			if (ret == OSTIMER_RESTART)
				mod_timer(&timer[i].kernel_timer,
					  jiffies + timer[i].timeout_jiffies);
			else
				timer[i].func = NULL;
		}
}

#if defined (DRIVER_TYPE_DSL)
static asmlinkage int os_gettimeofday(struct timeval *tv)
{
	if (tv)
		do_gettimeofday(tv);
	return 0;
}
#endif

static int nl_needed = 0;

static asmlinkage void os_printf(char *s, va_list args)
{
#ifdef DEBUG
	char	buffer[PRINTF_BUFFER_SIZE];
	char *	bufptr = buffer;
	int	count;

	if (nl_needed) {
		nl_needed = 0;
		printk ("\n");
	}	
	count = vsnprintf (bufptr, sizeof (buffer), s, args);
	if ('\n' == buffer[0]) {
		bufptr++;
	}
	if ('\n' != buffer[count - 1]) {
		assert (count < (int) (sizeof (buffer) - 2));
		buffer[count++] = '\n';
		buffer[count]   = (char) 0;
	}
	printk(KERN_INFO "%s", bufptr);
#endif
}

static asmlinkage void os_puts(char *str)
{
	printk("%s", str);
}
 
static asmlinkage void os_putl(long l)
{
	nl_needed = 1; 
	printk("%ld", l); 
}

static asmlinkage void os_puti(int i)
{
	nl_needed = 1; 
	printk("%d", i); 
}

static asmlinkage void os_putnl(void)
{
	nl_needed = 0; 
	printk("\n"); 
}

static asmlinkage void os_putc(char c)
{
	char buffer[10];

	nl_needed = 1;
	if ((31 < c) && (c < 127))
	        snprintf(buffer, 10, "'%c' (0x%02x)", c, (unsigned char) c);
	else
		snprintf(buffer, 10, "0x%02x", (unsigned char) c);
	printk("%s", buffer);
}

#if defined (DRIVER_TYPE_DSL_TM) || defined (DRIVER_TYPE_DSL_USB)

static asmlinkage void os_debug_printf(char *fmt, ...)
{
	char buffer[PRINTF_BUFFER_SIZE];
	va_list args;

	va_start (args, fmt);
	vsnprintf (buffer, sizeof (buffer), fmt, args);
	printk(KERN_INFO KBUILD_MODNAME ": %s", buffer);
	va_end (args);
}

#elif defined (DRIVER_TYPE_DSL_RAP)

static asmlinkage void os_debug_printf (char * fmt, va_list args)
{
	char buffer[PRINTF_BUFFER_SIZE];

	vsnprintf (buffer, sizeof (buffer), fmt, args);
	printk(KERN_INFO KBUILD_MODNAME ": %s", buffer);
}

#endif

#if !defined (DRIVER_TYPE_DSL)
void asmlinkage printl(char *fmt, ...)
{
	/* FIXME */

	va_list	args;
	char	buffer [PRINTF_BUFFER_SIZE];

	va_start (args, fmt);
	vsnprintf (buffer, sizeof (buffer), fmt, args);
	printk ("%s", buffer);
	va_end (args);
}
#endif

static lib_interface_t libif = {
	.init =				&os_init,
	.params =			&os_params,
	.get_message =			&os_get_message,
	.put_message =			&os_put_message,
	.get_data_block =		&os_get_data_block,
	.free_data_block =		&os_free_data_block,
	.new_ncci =			&os_new_ncci,
	.free_ncci =			&os_free_ncci,
	.block_size =			&os_block_size,
	.window_size =			&os_window_size,
	.card =				&os_card,
	.appl_data =			&os_appl_data,
#if defined (DRIVER_TYPE_DSL_RAP)
	.appl_attr =			&os_appl_attr,
#endif
	.appl_1st_data =		&os_appl_1st_data,
	.appl_next_data =		&os_appl_next_data,
	.malloc =			&os_malloc,
	.free =				&os_free,
#if defined (DRIVER_TYPE_DSL) && !defined (DRIVER_TYPE_DSL_USB)
	.malloc2 =			&os_malloc,
#endif
#if defined (DRIVER_TYPE_DSL_RAP) || defined (DRIVER_TYPE_DSL_USB)
	.delay =			&os_delay,
#endif
	.msec =				&os_msec,	
	.msec64 =			&os_msec64,
	.timer_new =			&os_timer_new,
	.timer_delete =			&os_timer_delete,
	.timer_start =			&os_timer_start,
	.timer_stop =			&os_timer_stop,
	.timer_poll =			&os_timer_poll,
#if defined (DRIVER_TYPE_DSL)
	.get_time =			&os_gettimeofday,
#endif
#if defined (DRIVER_TYPE_DSL_TM) || defined (DRIVER_TYPE_DSL_USB)
	.dprintf =			&os_debug_printf,
#endif
#if defined (DRIVER_TYPE_DSL_RAP)
	.printf =			&os_debug_printf,
	.putf =				&os_printf,
#else
	.printf =			&os_printf,
#endif
	.puts =				&os_puts,
	.putl =				&os_putl,
	.puti =				&os_puti,
	.putc =				&os_putc,
	.putnl =			&os_putnl,
	._enter_critical =		&os__enter_critical,
	._leave_critical =		&os__leave_critical,
	.enter_critical =		&os_enter_critical,
	.leave_critical =		&os_leave_critical,
	.enter_cache_sensitive_code =	&os_enter_cache_sensitive_code,	
	.leave_cache_sensitive_code =	&os_leave_cache_sensitive_code,

#if defined (DRIVER_TYPE_DSL_RAP)
	.xfer_req =			&dif_xfer_requirements,
#endif

	.name =				KBUILD_MODNAME,
	.udata =			0,
	.pdata =			NULL
};

lib_callback_t *link_library(int **ctr2_cnr_ptr)
{
#if defined (LOG_STACK_MSG)
	LOG_TRACE_MSG(CAPI_DATA_B3);
#endif
	fritz_dbg("Interface exchange... (%zu)\n", sizeof(lib_interface_t));
#if defined (DRIVER_TYPE_DSL)
	return avm_lib_attach(&libif, ctr2_cnr_ptr);
#else
	return avm_lib_attach(&libif);
#endif
}

void free_library(void)
{
	avm_lib_detach(&libif);
}
