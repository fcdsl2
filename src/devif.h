/*
 * AVM FRITZ!Card DSL v2.0 CAPI driver, open source part.
 *
 * Copyright 2003, AVM GmbH
 * Copyright 2010, Jan Kiszka <jan.kiszka@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, see
 * http://www.gnu.org/licenses/.
 */

#ifndef __have_devif_h__
#define __have_devif_h__

#include <linux/sched.h>
#include <linux/interrupt.h>
#include "common.h"
#include "driver.h"
#include "libdefs.h"

#define C6205_PCI_HSR_OFFSET		0
#define C6205_PCI_HDCR_OFFSET		4
#define C6205_PCI_DSPP_OFFSET		8

#define	C6205_PCI_HDCR_WARMRESET	1
#define	C6205_PCI_HDCR_DSPINT		2
#define	C6205_PCI_HDCR_PCIBOOT		4
#define	C6205_PCI_HSR_INTSRC		1
#define	C6205_PCI_HSR_INTAVAL		2
#define	C6205_PCI_HSR_OMTA		4
#define	C6205_PCI_HSR_CFRGERR		8
#define	C6205_PCI_HSR_EEREAD		16

void xfer_handler(struct fritz_card *card);

bool dif_init(void);
void dif_exit(void);

void dif_set_params(unsigned num, unsigned offset, ioaddr_p pwin,
		    unsigned len, unsigned bufofs);

asmlinkage void dif_xfer_requirements(dif_require_p rp, unsigned ofs);

irqreturn_t device_interrupt(int irq, void *args);

dma_struct_p dma_alloc(void);
void dma_free(dma_struct_p *ppdma);

bool dma_setup(dma_struct_p pdma, unsigned buf_size);
void dma_exit(dma_struct_p pdma);

dma_struct_p *dma_get_struct_list(unsigned *pnum);

#if defined (USB_EVENT_HARD_ERROR)
# undef USB_EVENT_HARD_ERROR
#endif

#define USB_REQUEST_CANCELLED	0x8000
#define USB_RX_REQUEST		0x4000
#define USB_TX_REQUEST		0x2000
#define USB_EVENT_HARD_ERROR	0x0800

#define DMA_REQUEST_CANCELLED	0x8000
#define DMA_RX_REQUEST		0x4000
#define DMA_TX_REQUEST		0x2000
#define DMA_EVENT_HARD_ERROR	0x0800

asmlinkage unsigned OSHWBlockOpen(unsigned index, hw_block_handle *handle);
asmlinkage void OSHWBlockClose(hw_block_handle handle);

asmlinkage unsigned
OSHWStart(hw_block_handle handle, hw_completion_func_t rx_completer,
	  hw_completion_func_t tx_completer, hw_event_func_t event_handler);

asmlinkage void OSHWStop(hw_block_handle handle);

asmlinkage hw_buffer_descriptor_p
OSHWAllocBuffer(hw_block_handle handle, unsigned length);

asmlinkage void OSHWFreeBuffer(hw_buffer_descriptor_p desc);

asmlinkage unsigned OSHWGetMaxBlockSize(hw_block_handle handle);
asmlinkage unsigned OSHWGetMaxConcurrentRxBlocks(hw_block_handle handle);
asmlinkage unsigned OSHWGetMaxConcurrentTxBlocks(hw_block_handle handle);

asmlinkage void *
OSHWExchangeDeviceRequirements(hw_block_handle handle, void *pin);

asmlinkage unsigned
OSHWTxBuffer(const hw_buffer_descriptor_p desc, unsigned offset,
	     unsigned length, void *context);

asmlinkage unsigned
OSHWRxBuffer(const hw_buffer_descriptor_p desc, unsigned offset,
	     unsigned length, void *context);

asmlinkage unsigned OSHWCancelRx(hw_block_handle handle);
asmlinkage unsigned OSHWCancelTx(hw_block_handle handle);

#endif
