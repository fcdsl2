include common.rules

ifeq ($(CONFIG_X86_64),y)
ARCHIVE ?= fcdsl2-suse93-64bit-3.11-07.tar.gz
else
ARCHIVE ?= fcdsl2-suse93-3.11-07.tar.gz
endif

all modules:: lib/$(FCDSL2_LIB)

all modules modules_install clean distclean::
	$(MAKE) -C src $@

distclean::
	rm -rf lib

lib/$(FCDSL2_LIB):
	@if ! test -f $(ARCHIVE); then \
		echo "Please copy $(ARCHIVE) into this directory."; \
		exit 1; \
	fi
	tar xzf $(ARCHIVE) --strip-components=1 fritz/lib/fcdsl2-lib.o
	mv lib/fcdsl2-lib.o lib/$(FCDSL2_LIB)
	objcopy -L memcmp -L memcpy -L memmove -L memset -L strcat -L strcmp \
		-L strcpy -L strlen -L strncmp -L strncpy lib/$(FCDSL2_LIB)

.PHONY: all modules modules_install clean distclean
